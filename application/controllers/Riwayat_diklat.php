<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Library\Datatable;
use App\Models\MriwayatDiklat;


class Riwayat_diklat extends USER_Controller {
	
	function index($jenis)
	{
		return view('pages.riwayat.diklat.index',['jenis'=>$jenis]);
	}
	
	function data()
	{
		$table = 'riwayat_diklat';
		
		$dt = new Datatable($table,['data_pegawai.nama','nama']);
		$select = "$table.*,data_pegawai.nama as nama_pegawai,b.nama as nama_struktural,c.nama as nama_fungsional,d.nama as nama_teknis";
		$join   = [
			['data_pegawai',"data_pegawai.id=$table.pegawai_id"],
			['master_diklat_struktural as b',"b.id=$table.struktural_id"],
			['master_diklat_fungsional as c',"c.id=$table.fungsional_id"],
			['master_diklat_teknis as d',"b.id=$table.teknis_id"]];
		$where["$table.jenis"] = 'DRAFT';
		
		echo json_encode($dt->get_datatables($select,$join,$where));
	}
	
	function form($id)
	{
		$row = MriwayatDiklat::find($id);
		return view('pages.riwayat.diklat.form',compact('id','row'));
	}
	
	function save()
	{
		$save = MriwayatDiklat::create([
			'pegawai_id' => _post('pegawai_id'),
			'struktural_id' => _post('struktural_id',0),
			'fungsional_id' => _post('fungsional_id',0),
			'teknis_id' => _post('teknis_id',0),
			'tanggal_mulai' => tanggal_sql(_post('tanggal_mulai')),
			'tanggal_selesai' => tanggal_sql(_post('tanggal_selesai')),
			'sttp_no' => _post('sttp_no'),
			'penyelenggara' => _post('penyelenggara'),
			'tempat' => _post('tempat'),
			'jenis_diklat' => _post('jenis_diklat'),
			'jumlah_jam' => _post('jumlah_jam'),
			'sttp_tanggal' => tanggal_sql(_post('sttp_tanggal')),
			'sttp_pejabat' => _post('sttp_pejabat'),
			'jenis' => 'DRAFT',
			'user_input' => session_get('user_id'),
			'tanggal_input' => date('Y-m-d'),
			'data_id' => _post('id',0),
			'keterangan_draft' => _post('id') == 0 ? 'Input Diklat Baru' : 'Edit Diklat'
		]);
		
		if ($save){
			echo 'success';
		} else {
			echo "Data Gagal Disimpan/Diubah";
		}
	}
	
	function hapus()
	{
		$id = _post('id');
		if (Mbahasa::destroy($id)){
			echo 'success';
			return;
		}
		echo "Data Gagal Dihapus";
	}
	
	
	
	function acc()
	{
		$id = _post('id');
		$row = MriwayatDiklat::find($id);
		if ($row){
			if ($row->data_id == 0){
				$data = $row->toArray();
				$data['jenis'] = 'DATA';
				unset($data['id']);
				MriwayatDiklat::create($data);
			} else {
				$riw = MriwayatDiklat::find($row->data_id);
				if ($riw){
					 $riw->tanggal_mulai = $row->tanggal_mulai;
					 $riw->tanggal_selesai = $row->tanggal_selesai;
					 $riw->sttp_no = $row->sttp_no;
					 $riw->sttp_tanggal = $row->sttp_tanggal;
					 $riw->sttp_pejabat = $row->sttp_pejabat;
					 $riw->penyelenggara = $row->penyelenggara;
					 $riw->jumlah_jam = $row->jumlah_jam;
					 $riw->tempat = $row->tempat;
					 $riw->save();
				}
			}
			
			$row->user_acc = session_get('user_id');
			$row->tanggal_acc = date('Y-m-d');
			$row->status_draft = 'ACC';
			$row->save();
			echo 'success';
			
		} else {
			echo "Data Gagal Diacc";
		}
	}
}