<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Models\Mdraft;
use App\Models\Mpegawai;

class Draft extends USER_Controller {
	
	function hapus()
	{
		$id = _post('id');
		if (Mdraft::destroy($id)){
			echo 'success';
			return;
		}
		echo "Data Gagal Dihapus";
	}
	
	function acc()
	{
		$id = _post('id');
		$draft = Mdraft::find($id);
		if ($draft){
			$draft->status = "VERIFIED";
			$draft->tanggal_acc = date('Y-m-d H:i:s');
			$draft->user_acc = 'bkd';
			if ($draft->save()){
				$row = json_decode($draft->data,true);
				//unset($row['nip']);
				Mpegawai::updateOrCreate(
					['id'=>$draft->pegawai_id],
					$row
				);
				echo 'success';
				return;
			}
		}
		
		echo 'Gagal Acc Data';
	}
}