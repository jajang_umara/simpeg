<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Library\Datatable;
use App\Models\Mbahasa;

class Bahasa extends ADMIN_Controller {
	
	function index()
	{
		return view('pages.master.bahasa.index');
	}
	
	function data()
	{
		$table = new Datatable('master_bahasa',['nama','deskripsi']);
		echo json_encode($table->get_datatables());
	}
	
	function form($id)
	{
		$row = Mbahasa::find($id);
		return view('pages.master.bahasa.form',compact('id','row'));
	}
	
	function save()
	{
		if (Mbahasa::updateOrCreate(
			['id'=>_post('id')],
				['nama'=>_post('nama'),'deskripsi'=>_post('deskripsi')])){
					echo "success";
			} else {
				echo "Data Gagal Disimpan/Edit";
			}
	}
	
	function hapus()
	{
		$id = _post('id');
		if (Mbahasa::destroy($id)){
			echo 'success';
			return;
		}
		echo "Data Gagal Dihapus";
	}
}