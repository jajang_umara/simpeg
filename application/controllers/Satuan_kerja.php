<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Library\Datatable;
use App\Models\MsatuanKerja;

class Satuan_kerja extends ADMIN_Controller {
	
	function index()
	{
		return view('pages.master.satuan_kerja.index');
	}
	
	function data()
	{
		$table = new Datatable('master_satuan_kerja',['nama','alamat']);
		echo json_encode($table->get_datatables());
	}
	
	function form($id)
	{
		$row = MsatuanKerja::find($id);
		return view('pages.master.satuan_kerja.form',compact('id','row'));
	}
	
	function save()
	{
		if (MsatuanKerja::updateOrCreate(
			['id'=>_post('id')],
				['nama'=>_post('nama'),'alamat'=>_post('alamat'),'status'=>_post('status',0)])){
					echo "success";
			} else {
				echo "Data Gagal Disimpan/Edit";
			}
	}
	
	function hapus()
	{
		$id = _post('id');
		if (Mbahasa::destroy($id)){
			echo 'success';
			return;
		}
		echo "Data Gagal Dihapus";
	}
}