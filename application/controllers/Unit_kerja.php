<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Library\Datatable;
use App\Models\MunitKerja;
use App\Models\MsatuanKerja;

class Unit_kerja extends ADMIN_Controller {
	
	function index()
	{
		$rows = [];
		$satuanKerja = [];
		if (_get('satuan_kerja') != ''){
			$rows = MunitKerja::with('childrens')->where('satuan_kerja_id',_get('satuan_kerja'))->where('parent',0)->get();
			$satuanKerja = MsatuanKerja::find(_get('satuan_kerja'));
		}
		
		$this->load->helper('unit_kerja');
		
		return view('pages.master.unit_kerja.index',compact('rows','satuanKerja'));
	}
	
	function form($id,$parent)
	{
		$row = MunitKerja::find($id);
		return view('pages.master.unit_kerja.form',compact('id','parent','row'));
	}
	
	function save()
	{
		if (MunitKerja::updateOrCreate(
			['id'=>_post('id')],
				['nama'=>_post('nama'),'parent'=>_post('parent',0),'status'=>_post('status',0),'satuan_kerja_id'=>_post('satuan_kerja_id')])){
					echo "success";
			} else {
				echo "Data Gagal Disimpan/Edit";
			}
	}
	
	function hapus()
	{
		$id = _post('id');
		if (MunitKerja::destroy($id)){
			echo 'success';
			return;
		}
		echo "Data Gagal Dihapus";
	}
	
	function get_unit()
	{
		$satuan_kerja = _post('satuan_kerja');
		$this->load->helper('unit_kerja');
		$html = "<option value='0'>-tanpa unit kerja-</option>";
		$html .= option_unit_kerja(MunitKerja::with('childrens')->where('satuan_kerja_id',$satuan_kerja)->where('parent',0)->get(),_post('unit_kerja'));
		echo $html;
	}
}