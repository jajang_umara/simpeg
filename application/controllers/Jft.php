<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Library\Datatable;
use App\Models\Mjft;

class Jft extends ADMIN_Controller {
	
	function index($type)
	{
		return view('pages.master.jft.index',compact('type'));
	}
	
	function data()
	{
		$table = new Datatable('master_jft',['nama','skill']);
		echo json_encode($table->get_datatables("*",[],["type"=>_post('type')]));
	}
	
	function form($id)
	{
		$row = Mjft::find($id);
		return view('pages.master.jft.form',compact('id','row'));
	}
	
	function save()
	{
		if (Mjft::updateOrCreate(
			['id'=>_post('id')],
				['nama'=>_post('nama'),'rumpun_id'=>_post('rumpun_id'),'jabatan_kategori_id'=>_post('jabatan_kategori_id'),'golongan_awal_id'=>_post('golongan_awal_id'),
				 'golongan_akhir_id'=>_post('golongan_akhir_id'),'skill'=>_post('skill'),'batas_usia_pensiun'=>_post('batas_usia_pensiun'),'kode_jabatan'=>_post('kode_jabatan'),
				 'kelas'=>_post('kelas'),'type'=>_post('type','TERTENTU'),'syarat'=>_post('syarat')
				])){
					echo "success";
			} else {
				echo "Data Gagal Disimpan/Edit";
			}
	}
	
	function get_kode($kelas){
		$kelas = sprintf("%02s",$kelas);
		$prefix = '3.000.';
		if (_get('type') == 'UMUM'){
			$prefix = '2.000.';
		}
		$jft = Mjft::where('kode_jabatan','like',$prefix.$kelas.'%')->orderBy('kode_jabatan','desc')->first();
		if (!$jft){
			 echo $prefix.$kelas.'.001';
			 return;
		}
		
		$kode = (int)substr($jft->kode_jabatan,-3);
		$kode++;
		echo $prefix.$kelas.sprintf("%03s",$kode);
	}
	
	function hapus()
	{
		$id = _post('id');
		if (Mjft::destroy($id)){
			echo 'success';
			return;
		}
		echo "Data Gagal Dihapus";
	}
	
	function get_list($type)
	{
		$rows = Mjft::where('type',$type)->orderBy('nama')->get();
		echo $rows ? $rows->toJson() : "[]";
	}
}