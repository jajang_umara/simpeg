<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Models\Mkabupaten;
use App\Models\MunitKerja;
use App\Models\MsatuanKerja;
use App\Models\Mpegawai;
use App\Models\Mdraft;

use App\Library\Datatable;

class Pegawai extends USER_Controller
{
	function index()
	{
		return view('pages.pegawai.index');
	}
	
	function belum_verifikasi()
	{
		return view('pages.pegawai.belum_verifikasi');
	}
	
	function data()
	{
		$table = new Datatable('data_pegawai',['nama','deskripsi']);
		$select = "data_pegawai.*,master_golongan.nama as nama_golongan,master_jabatan.nama as nama_jabatan,master_jft.nama as nama_jabatan2,master_jabatan.jenis";
		$join   = [["master_golongan","master_golongan.id=data_pegawai.gol_akhir_id"],
							 ["master_jabatan","master_jabatan.id=data_pegawai.jabatan_id"],
							 ["master_jft","master_jabatan.jf_id=master_jft.id"]];
		$where['data_pegawai.satuan_kerja_id'] = _post('satuan_kerja');
		echo json_encode($table->get_datatables($select,$join,$where));
	}
	
	function form($satuan_kerja)
	{
		$unitKerja = MunitKerja::with('childrens')->where('satuan_kerja_id',$satuan_kerja)->where('parent',0)->get();
		$satuanKerja = MsatuanKerja::find($satuan_kerja);
		$this->load->helper('unit_kerja');
		
		$data['unitKerja'] = $unitKerja;
		$data['satuanKerja'] = $satuanKerja;
		if (flash_get('row')){
			$data['row'] = json_decode(flash_get('row'));
		}
		
		if (_get('draft_id') != ''){
			$draft = Mdraft::find(_get('draft_id'));
			if ($draft){
				$data['row'] = json_decode($draft->data);
				$data['draft_id'] = $draft->id;
			}
		}
		
		if (_get('pegawai_id') != ''){
			$data['row'] = Mpegawai::with('satuan_kerja')->with('unit_kerja')->find(_get('pegawai_id'));
		}
		
		return view('pages.pegawai.form',$data);
	}
	
	function get_kabupaten($id)
	{
		$kab = Mkabupaten::where('id',$id)->value('nama');
		echo $kab;
	}
	
	function detail($id)
	{
		$pegawai = Mpegawai::with('satuan_kerja')->where('id',$id)->first();
		$data['pegawai'] = $pegawai;
		return view('pages.pegawai.detail',$data);
	}
}