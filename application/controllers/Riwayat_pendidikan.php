<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Library\Datatable;
use App\Models\MriwayatPendidikan;
use App\Models\Muniversitas;

class Riwayat_pendidikan extends USER_Controller {
	
	function index()
	{
		return view('pages.riwayat.pendidikan.index');
	}
	
	function data()
	{
		$table = new Datatable('riwayat_pendidikan',['data_pegawai.nama','nama_sekolah']);
		$select = "riwayat_pendidikan.*,data_pegawai.nama as nama_pegawai";
		$join   = [[
			'data_pegawai','data_pegawai.id=riwayat_pendidikan.pegawai_id'
		]];
		$where['riwayat_pendidikan.jenis'] = 'DRAFT';
		
		echo json_encode($table->get_datatables($select,$join,$where));
	}
	
	function form($id)
	{
		$row = MriwayatPendidikan::find($id);
		return view('pages.riwayat.pendidikan.form',compact('id','row'));
	}
	
	function save()
	{
		$save = MriwayatPendidikan::create([
			'pegawai_id' => _post('pegawai_id'),
			'tingkat_pendidikan_id' => _post('tingkat_pendidikan_id'),
			'fakultas_id' => _post('fakultas_id'),
			'jurusan_id' => _post('jurusan_id'),
			'nama_sekolah' => _post('nama_sekolah'),
			'lokasi' => _post('lokasi'),
			'no_ijazah' => _post('no_ijazah'),
			'tanggal_ijazah' => tanggal_sql(_post('tanggal_ijazah')),
			'pejabat' => _post('pejabat'),
			'jenis' => 'DRAFT',
			'user_input' => session_get('user_id'),
			'tanggal_input' => date('Y-m-d'),
			'data_id' => _post('id',0),
			'keterangan_draft' => _post('id') == 0 ? 'Input Pendidikan Baru' : 'Edit Pendidikan'
		]);
		
		if ($save){
			echo 'success';
		} else {
			echo "Data Gagal Disimpan/Diubah";
		}
	}
	
	function hapus()
	{
		$id = _post('id');
		if (Mbahasa::destroy($id)){
			echo 'success';
			return;
		}
		echo "Data Gagal Dihapus";
	}
	
	function universitas()
	{
		$rows = Muniversitas::where('nama','like','%'._get('term').'%')->orderBy('nama')->get();
		$result = [];
		if($rows){
			foreach($rows as $row){
				$result[] = array('value'=>$row->nama,'label'=>$row->nama,'id'=>$row->id);
			}
		}
		echo json_encode($result);
	}
	
	function acc()
	{
		$id = _post('id');
		$row = MriwayatPendidikan::find($id);
		if ($row){
			if ($row->data_id == 0){
				$data = $row->toArray();
				$data['jenis'] = 'DATA';
				unset($data['id']);
				MriwayatPendidikan::create($data);
			} else {
				$riw = MriwayatPendidikan::find($row->data_id);
				if ($riw){
					 $riw->tingkat_pendidikan_id = $row->tingkat_pendidikan_id;
					 $riw->jurusan_id = $row->jurusan_id;
					 $riw->fakultas_id = $row->fakultas_id;
					 $riw->nama_sekolah = $row->nama_sekolah;
					 $riw->lokasi = $row->lokasi;
					 $riw->no_ijazah = $row->no_ijazah;
					 $riw->tanggal_ijazah = $row->tanggal_ijazah;
					 $riw->pejabat = $row->pejabat;
					 $riw->save();
				}
			}
			
			$row->user_acc = session_get('user_id');
			$row->tanggal_acc = date('Y-m-d');
			$row->status_draft = 'ACC';
			$row->save();
			echo 'success';
			
		} else {
			echo "Data Gagal Diacc";
		}
	}
}