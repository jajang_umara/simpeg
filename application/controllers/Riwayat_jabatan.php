<?php
use App\Library\Datatable;
use App\Models\MriwayatJabatan;
use App\Models\Mpegawai;

class Riwayat_jabatan extends USER_Controller
{
	function index($status)
	{
		$data['status'] = $status;
		return view('pages.riwayat.jabatan.index',$data);
	}
	
	function data()
	{
		$table = new Datatable('riwayat_jabatan');
		$select = "riwayat_jabatan.*,master_satuan_kerja.nama as nama_satuan_kerja,
					  master_unit_kerja.nama as nama_unit_kerja,data_pegawai.nama,data_pegawai.nip,master_jabatan.nama as nama_jabatan,master_jft.nama as nama_jabatan2,master_jabatan.jenis";
		$join   = [["master_satuan_kerja","master_satuan_kerja.id=riwayat_jabatan.satuan_kerja_id"],
					  ["master_jabatan","master_jabatan.id=riwayat_jabatan.jabatan_id"],
					  ["master_jft","master_jft.id=master_jabatan.jf_id"],
					  ["master_unit_kerja","master_unit_kerja.id=riwayat_jabatan.unit_kerja_id"],
					  ["data_pegawai","data_pegawai.id=riwayat_jabatan.pegawai_id"]];
		$where  = [];
		if (_post('status') != ''){
			$where['riwayat_jabatan.status'] = _post('status');
		}
		echo json_encode($table->get_datatables($select,$join,$where));
	}
	
	function save()
	{
		$data = [
				'pegawai_id' => _post('pegawai_id'),
				'satuan_kerja_id' => _post('satuan_kerja_id'),
				'unit_kerja_id' => _post('unit_kerja_id'),
				'jabatan_id' => _post('jabatan_id'),
				'jabatan_tmt' => tanggal_sql(_post('jabatan_tmt')),
				'nomor_sk' => _post('nomor_sk'),
				'tanggal_sk' => tanggal_sql(_post('tanggal_sk')),
				'pejabat' => _post('pejabat')
			];
			
		if (isset($_FILES['lampiran']) && $_FILES['lampiran']['name'] != ''){
			$path = FCPATH.'uploads/lampiran/riwayat_jabatan/';
			$data['lampiran'] = $this->upload_file('lampiran',$path);
		}
		if (MriwayatJabatan::updateOrCreate(['id'=>_post('id')],$data)){
			echo 'success';
		} else {
			echo 'Data Gagal Disimpan';
		}
	}
	
	function acc_data()
	{
		$id = _post('id');
		if ($row = Mriwayatjabatan::find($id)){
			$pegawai = Mpegawai::find($row->pegawai_id);
			if (strtotime($pegawai->jabatan_tmt) < strtotime($row->jabatan_tmt)){
				$row->status = "ACC";
				$row->user_acc = 'bkd';
				$row->tanggal_acc = date('Y-m-d');
				$pegawai->satuan_kerja_id = $row->satuan_kerja_id;
				$pegawai->unit_kerja_id = $row->unit_kerja_id;
				$pegawai->jabatan_id = $row->jabatan_id;
				$pegawai->jabatan_tmt = $row->jabatan_tmt;
				$pegawai->save();
			} else {
				$row->status = "TIDAK ACC";
				$row->keterangan = "TMT riwayat lebih Kecil dari TMT pegawai";
			}
			
			$row->save();
			echo 'success';
			return;
		}
		echo "Data Gagal Diacc";
	}
}