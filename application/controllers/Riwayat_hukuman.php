<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Library\Datatable;
use App\Models\MriwayatHukuman;


class Riwayat_hukuman extends USER_Controller {
	
	function index()
	{
		return view('pages.riwayat.hukuman.index');
	}
	
	function data()
	{
		$table = new Datatable('riwayat_hukuman',['data_pegawai.nama','nama']);
		$select = "riwayat_hukuman.*,data_pegawai.nama as nama_pegawai,master_hukuman.jenis,master_hukuman.nama as nama_hukuman";
		$join   = [[
			'data_pegawai','data_pegawai.id=riwayat_hukuman.pegawai_id'],
			['master_hukuman','master_hukuman.id=riwayat_hukuman.hukuman_id']
		];
		$where['riwayat_hukuman.jenis'] = 'DRAFT';
		
		echo json_encode($table->get_datatables($select,$join,$where));
	}
	
	function form($id)
	{
		$row = MriwayatHukuman::find($id);
		return view('pages.riwayat.hukuman.form',compact('id','row'));
	}
	
	function save()
	{
		$save = MriwayatHukuman::create([
			'pegawai_id' => _post('pegawai_id'),
			'hukuman_id' => _post('hukuman_id'),
			'tanggal_mulai' => tanggal_sql(_post('tanggal_mulai')),
			'tanggal_selesai' => tanggal_sql(_post('tanggal_selesai')),
			'nomor_sk' => _post('nomor_sk'),
			'tanggal_sk' => tanggal_sql(_post('tanggal_sk')),
			'keterangan' => _post('keterangan'),
			'jenis' => 'DRAFT',
			'user_input' => session_get('user_id'),
			'tanggal_input' => date('Y-m-d'),
			'data_id' => _post('id',0),
			'keterangan_draft' => _post('id') == 0 ? 'Input Hukuman Baru' : 'Edit Hukuman'
		]);
		
		if ($save){
			echo 'success';
		} else {
			echo "Data Gagal Disimpan/Diubah";
		}
	}
	
	function hapus()
	{
		$id = _post('id');
		if (Mbahasa::destroy($id)){
			echo 'success';
			return;
		}
		echo "Data Gagal Dihapus";
	}
	
	
	
	function acc()
	{
		$id = _post('id');
		$row = MriwayatHukuman::find($id);
		if ($row){
			if ($row->data_id == 0){
				$data = $row->toArray();
				$data['jenis'] = 'DATA';
				unset($data['id']);
				MriwayatHukuman::create($data);
			} else {
				$riw = MriwayatHukuman::find($row->data_id);
				if ($riw){
					 $riw->hukuman_id = $row->hukuman_id;
					 $riw->tanggal_mulai = $row->tanggal_mulai;
					 $riw->tanggal_selesai = $row->tanggal_selesai;
					 $riw->nomor_sk = $row->nomor_sk;
					 $riw->tanggal_sk = $row->tanggal_sk;
					 $riw->keterangan = $row->keterangan;
					 $riw->save();
				}
			}
			
			$row->user_acc = session_get('user_id');
			$row->tanggal_acc = date('Y-m-d');
			$row->status_draft = 'ACC';
			$row->save();
			echo 'success';
			
		} else {
			echo "Data Gagal Diacc";
		}
	}
}