<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Models\Mpegawai;
use App\Models\Mdraft;

class Draft_pegawai extends USER_Controller
{
	
	function save_pegawai()
	{
		$data = [
			'satuan_kerja_id' => _post('satuan_kerja_id'),
			'unit_kerja_id' => _post('unit_kerja_id'),
			'nip' => _post('nip'),
			'nip_lama' => _post('nip_lama'),
			'nama' => _post('nama'),
			'nama_gelar_depan' => _post('nama_gelar_depan'),
			'nama_gelar_belakang' => _post('nama_gelar_belakang'),
			'tempat_lahir' => _post('tempat_lahir'),
			'tanggal_lahir' => tanggal_sql(_post('tanggal_lahir')),
			'jenis_kelamin' => _post('jenis_kelamin'),
			'status_perkawinan' => _post('status_perkawinan'),
			'agama' => _post('agama'),
			'status_kepegawaian' => _post('status_kepegawaian'),
			'cpns_tmt' => tanggal_sql(_post('cpns_tmt')),
			'pns_tmt' => tanggal_sql(_post('pns_tmt')),
			'pend_awal_id' => _post('pend_awal_id'),
			'pend_awal_th' => (int)_post('pend_awal_th'),
			'pend_akhir_id' => _post('pend_akhir_id'),
			'pend_akhir_th' => (int)_post('pend_akhir_th'),
			'jabatan_id' => _post('jabatan_id'),
			'jabatan_tmt' => tanggal_sql(_post('jabatan_tmt')),
			'ak' => _post('ak'),
			'instansi_dpk' => _post('instansi_dpk'),
			'gol_awal_id' => _post('gol_awal_id'),
			'gol_awal_tmt' => tanggal_sql(_post('gol_awal_tmt')),
			'gol_akhir_id' => _post('gol_akhir_id'),
			'gol_akhir_tmt' => tanggal_sql(_post('gol_akhir_tmt')),
			'kerja_tahun' => _post('kerja_tahun'),
			'kerja_bulan' => _post('kerja_bulan'),
			'karpeg' => _post('karpeg'),
			'karsutri' => _post('karsutri'),
			'no_askes' => _post('no_askes'),
			'ktp' => _post('ktp'),
			'npwp' => _post('npwp'),
			'goldar_id' => _post('goldar_id'),
			'bapertarum' => _post('bapertarum'),
			'tmt_kgb' => tanggal_sql(_post('tmt_kgb')),
			'alamat_rumah' => _post('alamat_rumah'),
			'desa' => _post('desa'),
			'kecamatan_id' => _post('kecamatan_id'),
			'kodepos' => _post('kodepos'),
			'telp' => _post('telp'),
			'telp_hp' => _post('telp_hp'),
			'email' => _post('email'),
			'tgl_input' => date('Y-m-d'),
		];
		
		foreach ($data as $key=>$val){
			if ($val == ""){
				unset($data[$key]);
			}
		}
		
		if (isset($_FILES['photo']) && $_FILES['photo']['name'] != ""){
			$path = FCPATH.'uploads/photo/';
			$data['photo'] = $this->upload_file('photo',$path);
		}
		
		Mdraft::updateOrCreate(
		['id'=>_post('draft_id')],
		[
			'pegawai_id' => _post('pegawai_id',0),
			'nip' => _post('nip'),
			'nama' => _post('nama'),
			'satuan_kerja_id' => _post('satuan_kerja_id'),
			'unit_kerja_id' => _post('unit_kerja_id'),
			'keterangan' => _post('pegawai_id') != "" ? 'Ubah Data Pegawai' :"Input Pegawai Baru",
			'tanggal_input' => date('Y-m-d H:i:s'),
			'table_name' => 'data_pegawai',
			'data' => json_encode($data)
		]);
		
		
		flash_set("message","Data Berhasil disimpan");
		redirect('pegawai/form/'.$data['satuan_kerja_id']);
	}
	
	function data_pegawai()
	{
		$rows = Mdraft::with('satuan_kerja')->with('unit_kerja')->whereNull('status')->where('satuan_kerja_id',_get('satuan_kerja'))->where('table_name','data_pegawai')->get();
		$data['rows'] = $rows;
		return view('pages.pegawai.data_draft',$data);
	}
}