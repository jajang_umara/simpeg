<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Library\Datatable;

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		//$datatable = new Datatable('master_bahasa');
		//echo json_encode($datatable->get_datatables());
		//$this->load->view('welcome_message');
		return view('pages.home.dashboard');
	}
}
