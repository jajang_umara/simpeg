<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Library\Datatable;
use App\Models\MunitKerja;
use App\Models\MsatuanKerja;
use App\Models\Mjabatan;

class Jabatan extends ADMIN_Controller {
	
	function index()
	{
		$rows = [];
		$satuanKerja = [];
		if (_get('satuan_kerja') != ''){
			$rows = MunitKerja::with('childrens')->where('satuan_kerja_id',_get('satuan_kerja'))->where('parent',0)->get();
			$satuanKerja = MsatuanKerja::find(_get('satuan_kerja'));
		}
		
		$this->load->helper('unit_kerja');
		
		$jabatan = Mjabatan::where('satuan_kerja_id',_get('satuan_kerja'))->where('unit_kerja_id',0)->with('eselon')->first();
		
		return view('pages.master.jabatan.index',compact('rows','satuanKerja','jabatan'));
	}
	
	function form($id)
	{
		$row = Mjabatan::find($id);
		return view('pages.master.jabatan.form',compact('id','row'));
	}
	
	function save()
	{
		if (_post('id') == 0){
			if (Mjabatan::where('satuan_kerja_id',_post('satuan_kerja_id'))->where('unit_kerja_id',_post('unit_kerja_id'))->where('jenis','STRUKTURAL')->count() > 0){
				echo "Jabatan Struktural Untuk Unit Kerja ini Sudah Diisi";
				return;
			}
		}
		
		if (Mjabatan::updateOrCreate(
			['id'=>_post('id')],
				['nama'=>_post('nama'),'satuan_kerja_id'=>_post('satuan_kerja_id'),'unit_kerja_id'=>_post('unit_kerja_id'),'jf_id'=>_post('jf_id'),
				'eselon_id'=>_post('eselon_id'),'kelas'=>_post('kelas'),'kode'=>_post('kode'),'jenis'=>_post('jenis')])){
					echo "success";
			} else {
				echo "Data Gagal Disimpan/Edit";
			}
	}
	
	function get_jabatan()
	{
		$jenis = _post('jenis');
		$satuan_kerja = _post('satuan_kerja');
		$unit_kerja = _post('unit_kerja');
		
		$jabatan = Mjabatan::with('jf')->where('jenis',$jenis)->where('satuan_kerja_id',$satuan_kerja)->where('unit_kerja_id',$unit_kerja)->get();
		
		echo $jabatan->toJson();
	}
	
	function hapus()
	{
		$id = _post('id');
		if (Mjabatan::destroy($id)){
			echo 'success';
			return;
		}
		echo "Data Gagal Dihapus";
	}
}