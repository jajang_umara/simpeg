<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Library\Datatable;
use App\Models\MriwayatKeluarga;


class Riwayat_keluarga extends USER_Controller {
	
	function index($jenis)
	{
		return view('pages.riwayat.keluarga.index',['jenis'=>$jenis]);
	}
	
	function data()
	{
		$table = 'riwayat_keluarga';
		
		$dt = new Datatable($table,['data_pegawai.nama','nama']);
		$select = "$table.*,data_pegawai.nama as nama_pegawai";
		$join   = [
			['data_pegawai',"data_pegawai.id=$table.pegawai_id"]];
		$where["$table.jenis"] = 'DRAFT';
		
		echo json_encode($dt->get_datatables($select,$join,$where));
	}
	
	function form($id)
	{
		$row = MriwayatKeluarga::find($id);
		return view('pages.riwayat.keluarga.form',compact('id','row'));
	}
	
	function save()
	{
		$save = MriwayatKeluarga::create([
			'pegawai_id' => _post('pegawai_id'),
			'hubungan' => _post('hubungan',1),
			'nama' => _post('nama'),
			'nik' => _post('nik'),
			'tempat_lahir' => _post('tempat_lahir'),
			'tanggal_lahir' => tanggal_sql(_post('tanggal_lahir')),
			'tanggal_nikah' => tanggal_sql(_post('tanggal_nikah')),
			'status_tunjangan' => _post('status_tunjangan',0),
			'pendidikan' => _post('pendidikan'),
			'pekerjaan' => _post('pekerjaan'),
			'jenis_kelamin' => _post('jenis_kelamin'),
			'status_perkawinan' => _post('status_perkawinan'),
			'keterangan_keluarga' => _post('keterangan_keluarga'),
			'jenis' => 'DRAFT',
			'user_input' => session_get('user_id'),
			'tanggal_input' => date('Y-m-d'),
			'data_id' => _post('id',0),
			'keterangan_draft' => _post('id') == 0 ? 'Input Keluarga Baru' : 'Edit Keluarga'
		]);
		
		if ($save){
			echo 'success';
		} else {
			echo "Data Gagal Disimpan/Diubah";
		}
	}
	
	function hapus()
	{
		$id = _post('id');
		if (Mbahasa::destroy($id)){
			echo 'success';
			return;
		}
		echo "Data Gagal Dihapus";
	}
	
	
	
	function acc()
	{
		$id = _post('id');
		$row = MriwayatKeluarga::find($id);
		if ($row){
			if ($row->data_id == 0){
				$data = $row->toArray();
				$data['jenis'] = 'DATA';
				unset($data['id']);
				MriwayatKeluarga::create($data);
			} else {
				$riw = MriwayatKeluarga::find($row->data_id);
				if ($riw){
					 $riw->nama = $row->nama;
					 $riw->nik = $row->nik;
					 $riw->jenis_kelamin = $row->jenis_kelamin;
					 $riw->tempat_lahir = $row->tempat_lahir;
					 $riw->tanggal_lahir = $row->tanggal_lahir;
					 $riw->tanggal_nikah = $row->tanggal_nikah;
					 $riw->status_tunjangan = $row->status_tunjangan;
					 $riw->pendidikan = $row->pendidikan;
					 $riw->pekerjaan = $row->pekerjaan;
					 $riw->status_perkawinan = $row->status_perkawinan;
					 $riw->keterangan_keluarga = $row->keterangan_keluarga;
					 $riw->save();
				}
			}
			
			$row->user_acc = session_get('user_id');
			$row->tanggal_acc = date('Y-m-d');
			$row->status_draft = 'ACC';
			$row->save();
			echo 'success';
			
		} else {
			echo "Data Gagal Diacc";
		}
	}
}