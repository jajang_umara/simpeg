<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Library\Datatable;
use App\Models\MriwayatNonformal;


class Riwayat_nonformal extends USER_Controller {
	
	function index()
	{
		return view('pages.riwayat.nonformal.index');
	}
	
	function data()
	{
		$table = new Datatable('riwayat_nonformal',['data_pegawai.nama','nama']);
		$select = "riwayat_nonformal.*,data_pegawai.nama as nama_pegawai";
		$join   = [[
			'data_pegawai','data_pegawai.id=riwayat_nonformal.pegawai_id'
		]];
		$where['riwayat_nonformal.jenis'] = 'DRAFT';
		
		echo json_encode($table->get_datatables($select,$join,$where));
	}
	
	function form($id)
	{
		$row = MriwayatNonformal::find($id);
		return view('pages.riwayat.nonformal.form',compact('id','row'));
	}
	
	function save()
	{
		$save = MriwayatNonformal::create([
			'pegawai_id' => _post('pegawai_id'),
			'nama' => _post('nama'),
			'tanggal_mulai' => tanggal_sql(_post('tanggal_mulai')),
			'tanggal_selesai' => tanggal_sql(_post('tanggal_selesai')),
			'sttp' => _post('sttp'),
			'penyelenggara' => _post('penyelenggara'),
			'tempat' => _post('tempat'),
			'sttp_tanggal' => tanggal_sql(_post('sttp_tanggal')),
			'sttp_pejabat' => _post('sttp_pejabat'),
			'jenis' => 'DRAFT',
			'user_input' => session_get('user_id'),
			'tanggal_input' => date('Y-m-d'),
			'data_id' => _post('id',0),
			'keterangan_draft' => _post('id') == 0 ? 'Input Pendidikan Baru' : 'Edit Pendidikan'
		]);
		
		if ($save){
			echo 'success';
		} else {
			echo "Data Gagal Disimpan/Diubah";
		}
	}
	
	function hapus()
	{
		$id = _post('id');
		if (Mbahasa::destroy($id)){
			echo 'success';
			return;
		}
		echo "Data Gagal Dihapus";
	}
	
	
	
	function acc()
	{
		$id = _post('id');
		$row = MriwayatNonformal::find($id);
		if ($row){
			if ($row->data_id == 0){
				$data = $row->toArray();
				$data['jenis'] = 'DATA';
				unset($data['id']);
				MriwayatNonformal::create($data);
			} else {
				$riw = MriwayatNonformal::find($row->data_id);
				if ($riw){
					 $riw->nama = $row->nama;
					 $riw->tanggal_mulai = $row->tanggal_mulai;
					 $riw->tanggal_selesai = $row->tanggal_selesai;
					 $riw->sttp = $row->sttp;
					 $riw->sttp_tanggal = $row->sttp_tanggal;
					 $riw->sttp_pejabat = $row->sttp_pejabat;
					 $riw->penyelenggara = $row->penyelenggara;
					 $riw->tempat = $row->tempat;
					 $riw->save();
				}
			}
			
			$row->user_acc = session_get('user_id');
			$row->tanggal_acc = date('Y-m-d');
			$row->status_draft = 'ACC';
			$row->save();
			echo 'success';
			
		} else {
			echo "Data Gagal Diacc";
		}
	}
}