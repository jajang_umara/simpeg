<?php
use App\Models\MsatuanKerja;
use App\Models\Mjabatan;
use App\Models\Mgolongan;

class Rekap extends USER_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('unit_kerja');
	}
	function skpd()
	{
		$skpd = new MsatuanKerja;
		$rows = $skpd->rekapPegawai();
		$data['rows'] = $rows;
		return view('rekap.skpd',$data);
	}
	
	function jabatan()
	{
		$skpd = new MsatuanKerja;
		$rows = $skpd->rekapJabatan();
		return view('rekap.jabatan',compact('rows'));
	}
	
	function golongan()
	{
		$skpd = new MsatuanKerja;
		$rows = $skpd->rekapGolongan();
		$golongan = Mgolongan::orderBy("nama");
		return view('rekap.golongan',compact('rows','golongan'));
	}
	
	function jenis_kelamin()
	{
		$skpd = new MsatuanKerja;
		$rows = $skpd->rekapJenisKelamin();
		return view('rekap.jenis_kelamin',compact('rows'));
	}
}