<?php
use App\Library\Datatable;
use App\Models\MriwayatPangkat;
use App\Models\Mpegawai;

class Riwayat_pangkat extends USER_Controller
{
	function index($status)
	{
		$data['status'] = $status;
		return view('pages.riwayat.pangkat.index',$data);
	}
	
	function data()
	{
		$table = new Datatable('riwayat_pangkat');
		$select = "riwayat_pangkat.*,master_golongan.nama as nama_golongan,master_satuan_kerja.nama as nama_satuan_kerja,
					  master_unit_kerja.nama as nama_unit_kerja,data_pegawai.nama,data_pegawai.nip";
		$join   = [["master_golongan","master_golongan.id=riwayat_pangkat.golongan_id"],
					  ["master_satuan_kerja","master_satuan_kerja.id=riwayat_pangkat.satuan_kerja_id"],
					  ["master_unit_kerja","master_unit_kerja.id=riwayat_pangkat.unit_kerja_id"],
					  ["data_pegawai","data_pegawai.id=riwayat_pangkat.pegawai_id"]];
		$where  = [];
		if (_post('status') != ''){
			$where['riwayat_pangkat.status'] = _post('status');
		}
		echo json_encode($table->get_datatables($select,$join,$where));
	}
	
	function save()
	{
		$data = [
				'pegawai_id' => _post('pegawai_id'),
				'satuan_kerja_id' => _post('satuan_kerja_id'),
				'unit_kerja_id' => _post('unit_kerja_id'),
				'golongan_id' => _post('golongan_id'),
				'golongan_tmt' => tanggal_sql(_post('golongan_tmt')),
				'pangkat_tahun' => _post('pangkat_tahun'),
				'pangkat_bulan' => _post('pangkat_bulan'),
				'nomor_sk' => _post('nomor_sk'),
				'tanggal_sk' => tanggal_sql(_post('tanggal_sk')),
				'pangkat_pejabat' => _post('pangkat_pejabat')
			];
			
		if (isset($_FILES['lampiran']) && $_FILES['lampiran']['name'] != ''){
			$path = FCPATH.'uploads/lampiran/riwayat_pangkat/';
			$data['lampiran'] = $this->upload_file('lampiran',$path);
		}
		if (MriwayatPangkat::updateOrCreate(['id'=>_post('id')],$data)){
			echo 'success';
		} else {
			echo 'Data Gagal Disimpan';
		}
	}
	
	function acc_data()
	{
		$id = _post('id');
		if ($row = MriwayatPangkat::find($id)){
			$pegawai = Mpegawai::find($row->pegawai_id);
			if (strtotime($pegawai->gol_akhir_tmt) <= strtotime($row->golongan_tmt)){
				$row->status = "ACC";
				$row->user_acc = session_get('user_id');
				$row->tanggal_acc = date('Y-m-d');
				$pegawai->gol_akhir_id = $row->golongan_id;
				$pegawai->gol_akhir_tmt = $row->golongan_tmt;
				$pegawai->kerja_tahun = $row->pangkat_tahun;
				$pegawai->kerja_bulan = $row->pangkat_bulan;
				$pegawai->save();
			} else {
				$row->status = "TIDAK ACC";
				$row->keterangan = "TMT riwayat lebih Kecil dari TMT pegawai";
			}
			
			$row->save();
			echo 'success';
			return;
		}
		echo "Data Gagal Diacc";
	}
}