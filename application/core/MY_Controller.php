<?php

class MY_Controller extends CI_Controller
{
	function upload_file($file_name, $path=null, $types = 'gif|jpg|jpeg|png|pdf|zip|rar|xls|xlsx|doc|docx') {
		$config = array();
		$config['upload_path'] = $path == null ? FCPATH.'uploads/' : $path;
		$config['allowed_types'] = $types;
		//$config['max_size']    = '15000000';

		$this->load->library('upload');

		$this->upload->initialize($config);

		if(!is_dir($path)){
			$old = umask(0);
			@mkdir($path, 0777, true);
			umask($old);
		}

		if ( $this->upload->do_upload($file_name)) {
			$data = $this->upload->data();
			return $data['file_name'];
			
		} else {
			throw new Exception($this->upload->display_errors());
		}
	}
}

class USER_Controller extends MY_Controller
{
	
}

class ADMIN_Controller extends USER_Controller
{
	
}