@extends('layouts.main')
@push('styles')
	<link href="{{ asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@endpush
@section('content')
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Master Satuan Kerja <small></small></h3>
			</div>
		</div>
		<div class="clearfix"></div>
		
		<div class="row">
			<div class="col-md-12 col-sm-12 ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Master Satuan Kerja</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><button class="btn btn-primary" onclick="loadForm(0)"><i class="fa fa-plus"></i> Tambah</button></li>
							
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="row">
								<div class="col-sm-12">
									<div class="card-box table-responsive">
										
										<table id="myTable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" style="width:100%">
											<thead>
												<tr>
													<th>Aksi</th>
													<th>Nama</th>
													<th>Alamat</th>
													<th>Status</th>
												</tr>
											</thead>
												
										</table>
									</div>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
							
	</div>
@endsection

@section('modal')
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-outline-info proses-button" onclick="doSave()" id="simpan">SIMPAN</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
	<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script>
		$(function(){
			table = $('#myTable').DataTable({
				"processing": true,
				"serverSide": true,
				"ajax": {
					"url": "{{ URL('satuan_kerja/data') }}",
					"type":"POST",
					"data": function ( d ) {
					}
				},
				columns:[
					{ "data": "aksi","orderable":false,"render":function(data,type,row){
						var button = '<button onclick="loadForm('+row.id+')" class="btn btn-outline-info btn-sm" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>&nbsp;'; 
						    button += '<button onclick="delData('+row.id+')" class="btn btn-outline-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></button>'
							return button;
					}},
					{ "data": "nama" },
					{ "data": "alamat" },
					{ "data": "status","render" : function(data){
						return data == 0 ? '<span class="bg-danger p-2 text-white">Non Aktif</span>' : '<span class="bg-success p-2 text-white">Aktif</span>'; 
					} },
				],
				"order": [[1, 'asc']],
			});
		});
		
		function loadForm(id)
		{
			$('.modal-title').text('TAMBAH/EDIT MASTER SATUAN KERJA');
			$('.modal-body').html('');
			$.post("{{ URL('satuan_kerja/form') }}/"+id).done(function(result){
				$('#myModal').modal('show');
				$('.modal-body').html(result);
			}).fail(function(xhr){
				$('#myModal').modal('show');
				alert(xhr.responseText)
			});
		}
		
		function doSave()
		{
			if (form.valid()){
				$('#simpan').prop('disabled',true)
				$.post("{{ site_url('satuan_kerja/save') }}",form.serialize()).done(function(resp){
					$('#simpan').prop('disabled',false)
					if (resp == 'success') {
						table.draw(false);
						$('#myModal').modal('hide');
						Toast.fire({
							icon:'success',
							title:"Data Berhasil Disimpan/Diubah"
						})
					} else {
						Toast.fire({
							icon:'error',
							title:resp
						})
					}
				}).fail(function(xhr){
					$('#simpan').prop('disabled',false);
					alert(xhr.responseText);
				})
			}
		}
		
		function delData(id)
		{
			Swal.fire({
				title: "Apakah Yakin akan menghapus data ini ?",
				icon: "warning",
				showCancelButton: true,
				confirmButtonText: 'Hapus',
			}).then(function(isConfirm){
				if (isConfirm.value) {
					$.post("{{ site_url('bahasa/hapus') }}",{id:id}).done(function(result){
						if (result == 'success') {
							table.draw(false);
							Toast.fire({icon:'success',title:"Data Berhasil Dihapus"})
							table.draw();
						} else {
							Toast.fire({icon:'error',title:result})
						}
					}).fail(function(xhr){
						Toast.fire({icon:'error',title:xhr.responseText})
					})
				}
			});
		}
	</script>
@endpush