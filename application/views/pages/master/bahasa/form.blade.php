@php use App\Library\FormBootstrap as Form; @endphp
<form id="form">
	{!! Form::hidden(['name'=>'id','value'=>@$id]) !!}
	{!! Form::input('Nama Bahasa',['name'=>'nama','class'=>'form-control','required'=>'required','value'=>@$row->nama]) !!}
	{!! Form::input('Deskripsi',['name'=>'deskripsi','class'=>'form-control','required'=>'required','value'=>@$row->deskripsi]) !!}
</form>
<script>
	var form = $('#form');
	form.validate();
	
</script>