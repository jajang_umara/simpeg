@php use App\Library\FormBootstrap as Form; @endphp
<form id="form">
	<div class="row">
		<div class="col-md-6">
			{!! Form::hidden(['name'=>'id','value'=>@$id]) !!}
			{!! Form::hidden(['name'=>'type','value'=>_post('type','TERTENTU')]) !!}
			{!! Form::input('Nama Jabatan',['name'=>'nama','class'=>'form-control','required'=>'required','value'=>@$row->nama]) !!}
			@if(_post('type') == 'TERTENTU')
			{!! Form::select('Rumpun',['name'=>'rumpun_id','class'=>'form-control','value'=>@$row->rumpun_id,'options'=>App\Models\MrumpunJabatan::pluck('nama','id')->prepend("--Pilih Rumpun--","")]) !!}
			{!! Form::select('Kategori',['name'=>'jabatan_kategori_id','class'=>'form-control','value'=>@$row->jabatan_kategori_id,'options'=>App\Models\MkategoriJabatan::pluck('nama','id')->prepend("--Pilih Kategori--","")]) !!}
			@endif
			{!! Form::select('Golongan Awal',['name'=>'golongan_awal_id','class'=>'form-control','value'=>@$row->golongan_awal_id,'options'=>App\Models\Mgolongan::pluck('nama','id')->prepend("--Pilih Golongan--","")]) !!}
			{!! Form::select('Golongan Akhir',['name'=>'golongan_akhir_id','class'=>'form-control','value'=>@$row->golongan_akhir_id,'options'=>App\Models\Mgolongan::pluck('nama','id')->prepend("--Pilih Golongan--","")]) !!}
		</div>
		<div class="col-md-6">
			{!! Form::input('Batas Usia Pensiun',['name'=>'batas_usia_pensiun','class'=>'form-control','required'=>'required','value'=>@$row->batas_usia_pensiun]) !!}
			@if(_post('type') == 'TERTENTU')
			{!! Form::select('Skill',['name'=>'skill','class'=>'form-control','value'=>@$row->skill,'options'=>['Terampil'=>'Terampil','Ahli'=>'Ahli']]) !!}
			@else
			{!! Form::input('Syarat',['name'=>'syarat','class'=>'form-control','value'=>@$row->syarat]) !!}
			@endif
			{!! Form::select('Kelas',['name'=>'kelas','class'=>'form-control','onchange'=>'getKodeJab(this.value)','value'=>@$row->kelas,'options'=>App\Models\MkelasJabatan::where('kelas','REGEXP','^[0-9]+$')->pluck('kelas','kelas')->prepend("--Pilih Kelas--","")]) !!}
			{!! Form::input('Kode Jabatan',['name'=>'kode_jabatan','class'=>'form-control','required'=>'required','readonly'=>'readonly','value'=>@$row->kode_jabatan]) !!}
		</div>
	</div>
</form>
<script>
	var form = $('#form');
	form.validate();
	new Switchery($('.js-switch')[0], {color: '#26B99A'});
	
	function getKodeJab(kelas)
	{
		$.get("{{ URL('jft/get_kode') }}/"+kelas,{type:"{{ _post('type') }}"}).done(function(resp){
			$('#kode_jabatan').val(resp);
		});
	}
</script>