@php use App\Library\FormBootstrap as Form; @endphp
<form id="form">
	{!! Form::hidden(['name'=>'id','value'=>@$id]) !!}
	{!! Form::hidden(['name'=>'parent','value'=>@$parent]) !!}
	{!! Form::hidden(['name'=>'satuan_kerja_id','value'=>_post('satuan_kerja')]) !!}
	<div class="form-group row">
		<label class="control-label col-md-3 col-sm-3 ">Status</label>
		<div class="col-md-9 col-sm-9 ">
			<div class="">
				<label>
					<input type="checkbox" class="js-switch" name="status" value="1" @if(isset($row)) @if($row->status == 1) checked @endif @else : checked @endif /> Aktif
				</label>
			</div>
		</div>
	</div>
	@if ($parent != 0)
	{!! Form::input('Nama Unit Kerja Induk',['name'=>'nama_parent','class'=>'form-control','disabled'=>'disabled','value'=>@App\Models\MunitKerja::find($parent)->nama]) !!}
	@endif
	{!! Form::input('Nama Unit Kerja',['name'=>'nama','class'=>'form-control','required'=>'required','value'=>@$row->nama]) !!}
	
</form>
<script>
	var form = $('#form');
	form.validate();
	new Switchery($('.js-switch')[0], {color: '#26B99A'});
</script>