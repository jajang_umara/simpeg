@extends('layouts.main')
@section('content')
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Master Jabatan <small></small></h3>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Master Jabatan</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<form method="get" id="form-satker">
							<div class="form-group row">
								<label class="control-label col-md-3 col-sm-3 ">Pilih Satuan Kerja</label>
								<div class="col-md-6 col-sm-6 ">
									<select class="select2 form-control" tabindex="-1" name="satuan_kerja" required onchange="$('#form-satker').submit()">
										<option value="">Pilih Satuan Kerja</option>
										@foreach(App\Models\MsatuanKerja::where('status',1)->get() as $row)
										<option value="{{ $row->id }}" @if(_get('satuan_kerja') == $row->id) selected @endif >{{ $row->nama }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</form>
						
						@if($rows)
							<table class="table table-bordered">
								<tr>
									
									<th>Unit Kerja</th>
									<th>Jabatan</th>
									<th>Jumlah Pegawai</th>
									<th>Aksi</th>
								</tr>
								<tr>
									<td>{{ @$satuanKerja->nama }}<br>
										<button class="btn btn-primary btn-sm" onclick="loadForm(0,{{ $satuanKerja->id }},0)"><i class="fa fa-plus"></i></button>
									</td>
									<td>{{ $jabatan ? $jabatan->nama : "-" }} ({{ $jabatan ? $jabatan->kode : "-"}}) ({{ $jabatan ? "eselon:".$jabatan->eselon->nama : "-" }})</td>
									<td></td>
									<td>@if($jabatan)
												<a href="#" onclick="loadForm({{ $jabatan->id }},{{ $satuanKerja->id }},0)"><i class="fa fa-edit" ></i> Edit</a> | 
												<a href='#' onclick='delData({{ $jabatan->id }})'><i class='fa fa-trash' ></i> Hapus</a>
										 @endif
									</td>
								</tr>
								{!! render_jabatan($rows) !!}
							</table>
						@endif
						
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('modal')
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-outline-info proses-button" onclick="doSave()" id="simpan">SIMPAN</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
	<script>
		function loadForm(id,satuanKerja,unitKerja)
		{
			$('.modal-title').text('TAMBAH/EDIT MASTER JABATAN');
			$('.modal-body').html('');
			$.post("{{ URL('jabatan/form') }}/"+id,{satuan_kerja:satuanKerja,unit_kerja:unitKerja}).done(function(result){
				$('#myModal').modal('show');
				$('.modal-body').html(result);
			}).fail(function(xhr){
				$('#myModal').modal('show');
				alert(xhr.responseText)
			});
		}
		
		function doSave()
		{
			if (form.valid()){
				$('#simpan').prop('disabled',true)
				$.post("{{ site_url('jabatan/save') }}",form.serialize()).done(function(resp){
					$('#simpan').prop('disabled',false)
					if (resp == 'success') {
						Toast.fire({icon:'success',title:"Data Berhasil Disimpan/Diedit"})
						location.reload();
					} else {
						Toast.fire({
							icon:'error',
							title:resp
						})
					}
				}).fail(function(xhr){
					$('#simpan').prop('disabled',false);
					alert(xhr.responseText);
				})
			}
		}
		
		function delData(id)
		{
			Swal.fire({
				title: "Apakah Yakin akan menghapus data ini ?",
				icon: "warning",
				showCancelButton: true,
				confirmButtonText: 'Hapus',
			}).then(function(isConfirm){
				if (isConfirm.value) {
					$.post("{{ site_url('unit_kerja/hapus') }}",{id:id}).done(function(result){
						if (result == 'success') {
							
							Toast.fire({icon:'success',title:"Data Berhasil Dihapus"})
							location.reload()
						} else {
							Toast.fire({icon:'error',title:result})
						}
					}).fail(function(xhr){
						Toast.fire({icon:'error',title:xhr.responseText})
					})
				}
			});
		}
		
		function delData(id)
		{
			Swal.fire({
				title: "Apakah Yakin akan menghapus data ini ?",
				icon: "warning",
				showCancelButton: true,
				confirmButtonText: 'Hapus',
			}).then(function(isConfirm){
				if (isConfirm.value) {
					$.post("{{ site_url('jabatan/hapus') }}",{id:id}).done(function(result){
						if (result == 'success') {
							Toast.fire({icon:'success',title:"Data Berhasil Dihapus"})
							location.reload()
						} else {
							Toast.fire({icon:'error',title:result})
						}
					}).fail(function(xhr){
						Toast.fire({icon:'error',title:xhr.responseText})
					})
				}
			});
		}
	</script>
@endpush