@php use App\Library\FormBootstrap as Form; @endphp
<form id="form">
	{!! Form::hidden(['name'=>'id','value'=>@$id]) !!}
  {!! Form::hidden(['name'=>'satuan_kerja_id','value'=>_post('satuan_kerja')]) !!}
	{!! Form::hidden(['name'=>'unit_kerja_id','value'=>_post('unit_kerja')]) !!}
	{!! Form::input('Satuan Kerja',['name'=>'nama_parent','class'=>'form-control','disabled'=>'disabled','value'=>@App\Models\MsatuanKerja::find(_post('satuan_kerja'))->nama]) !!}
	{!! Form::input('Unit Kerja',['name'=>'nama_parent','class'=>'form-control','disabled'=>'disabled','value'=>@App\Models\MunitKerja::find(_post('unit_kerja'))->nama]) !!}
	{!! Form::select('Jenis Jabatan',['name'=>'jenis','class'=>'form-control','required'=>'required','onchange'=>'changeJenis(this.value)','value'=>@$row->jenis,'options'=>jenis_jabatan()]) !!}
	<span class="struktural" style="display:none">
		{!! Form::input('Nama Jabatan',['name'=>'nama','class'=>'form-control','value'=>@$row->nama,'required'=>'required']) !!}
		{!! Form::select('Eselon',['name'=>'eselon_id','class'=>'form-control','required'=>'required','value'=>@$row->eselon_id,'options'=>App\Models\Meselon::pluck('nama','id')->prepend("-Pilih Eselon-","")]) !!}
		{!! Form::input('Kelas Jabatan',['name'=>'kelas','class'=>'form-control','value'=>@$row->kelas]) !!}
		{!! Form::input('Kode Jabatan',['name'=>'kode','class'=>'form-control','value'=>@$row->kode,'required'=>'required']) !!}
	</span>
	<span class="non-struktural" style="display:none">
		{!! Form::select('Nama Jabatan',['name'=>'jf_id','class'=>'form-control','required'=>'required','value'=>@$row->jf_id,'options'=>[""=>'-Pilih Jabatan-']]) !!}
	</span>
</form>
<script>
	var form = $('#form');
	form.validate();
	@if($row)
	changeJenis("{{ $row->jenis }}");
	@endif
	
	function changeJenis(value)
	{
		if (value == "STRUKTURAL"){
			$('.struktural').show();
			$('.non-struktural').hide();
		} else {
			$('.struktural').hide();
			$('.non-struktural').show();
			$.get("{{ URL('jft/get_list') }}/"+value).done(function(resp){
				resp = JSON.parse(resp);
				if (resp.length > 0){
					$('[name="jf_id"]').empty()
					$('[name="jf_id"]').append("<option value=''>-Pilih Jabatan-</option>");
					$.each(resp,function(k,v){
						$('[name="jf_id"]').append("<option value='"+v.id+"'>"+v.nama+"</option>");
					})
				}
			})
		}
	}
	
</script>