@php 
	use App\Models\MtingkatPendidikan; 
	use App\Models\Mjurusan;
	use App\Models\Mfakultas;
@endphp
<form id="form-pendidikan">
	<input type="hidden" name="id" value="{{ @$id }}">
	<input type="hidden" name="pegawai_id" value="{{ _post('pegawai_id') }}">
	<div class="form-group row">
		<label class="col-md-3">Tingkat Pendidikan</label>
		<div class="col-md-9">
			<select name="tingkat_pendidikan_id" class="form-control" onchange="tingkatChange()" id="tingkat_pendidikan_id" required>
				<option value="">-Pilih Tingkat Pendidikan-</option>
				@foreach(MtingkatPendidikan::all() as $r)
				<option value="{{ $r->id }}" data-urutan="{{ $r->urutan }}" @if(isset($row) && $row->tingkat_pendidikan_id == $r->id) selected @endif >{{ $r->nama }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-3">Fakultas</label>
		<div class="col-md-9">
			<select name="fakultas_id" class="form-control">
				<option value="">-Pilih Fakultas-</option>
				@foreach(Mfakultas::orderBy('nama')->get() as $r)
				<option value="{{ $r->id }}" @if(isset($row) && $row->fakultas_id == $r->id) selected @endif>{{ $r->nama }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-3">Nama Universitas/Sekolah</label>
		<div class="col-md-9">
			<input type="text" name="nama_sekolah" class="form-control" id="nama_sekolah" required value="{{ @$row->nama_sekolah }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-3">Lokasi</label>
		<div class="col-md-9">
			<input type="text" name="lokasi" class="form-control" required value="{{ @$row->lokasi }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-3">Jurusan</label>
		<div class="col-md-9">
			<select name="jurusan_id" class="form-control">
				<option value="">-Pilih Jurusan-</option>
				@foreach(Mjurusan::orderBy('nama')->get() as $r)
				<option value="{{ $r->id }}" @if(isset($row) && $row->jurusan_id == $r->id) selected @endif>{{ $r->nama }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-3">Nomor Ijazah/STTB</label>
		<div class="col-md-9">
			<input type="text" name="no_ijazah" class="form-control" required value="{{ @$row->no_ijazah }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-3">Tanggal Ijazah/STTB</label>
		<div class="col-md-9">
			<input type="text" name="tanggal_ijazah" class="form-control datepicker" required value="{{ @tanggal_sql($row->tanggal_ijazah) }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-3">Nama Kepala Sekolah/Rektor</label>
		<div class="col-md-9">
			<input type="text" name="pejabat" class="form-control" required value="{{ @$row->pejabat }}">
		</div>
	</div>
</form>

<script>
	function tingkatChange(){
		if (parseInt($('#tingkat_pendidikan_id option:selected').data('urutan')) > 3){
			$( "#nama_sekolah" ).autocomplete({
				source: "{{ URL('riwayat_pendidikan/universitas') }}",
				minLength: 4,
			})
			$('[name="fakultas_id"]').prop('disabled',false);
		} else {
			$('[name="fakultas_id"]').prop('disabled',true);
		}
	}
	
	$('.datepicker').daterangepicker({
		singleDatePicker:true,
		locale: {
			format:"DD-MM-YYYY",
		}
	})
	
	$('#form-pendidikan').validate({
		errorPlacement:function (error, element) {
			error.addClass('invalid-feedback');
			element.closest('div').append(error);
		}
	});
	
</script>

<style>
	ul.ui-autocomplete {
    z-index: 1100;
}
</style>
