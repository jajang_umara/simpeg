@php use App\Models\MriwayatPendidikan @endphp
<button class="btn btn-primary float-right" onclick="loadForm(0)"><i class="fa fa-plus"></i> Tambah</button>
<div class="table-responsive">
<table id="tabel-riwayat-pendidikan-formal" class="table table-bordered" cellspacing="0" style="width:1000px">
	<thead>
		<tr>
			<th rowspan="2">No.</th>
			<th rowspan="2">Tingkat Pendidikan</th>
			<th rowspan="2">Fakultas</th>
			<th rowspan="2">Jurusan</th>
			<th colspan="3"><center>STTB/Ijazah</center></th>
			<th colspan="2"><center>Sekolah/Perguruan Tinggi</center></th>
			<th rowspan="2"><center>Aksi</center></th>
		</tr>
		<tr>
			<th>Nomor</th>
			<th>Tanggal</th>
			<th>Nama Kepala Sekolah/Rektor</th>
			<th>Nama</th>
			<th>Lokasi (Kab./Kota)</th>
		</tr>
	</thead>
	@forelse(MriwayatPendidikan::where('pegawai_id',$pegawai->id)->where('jenis','DATA')->orderBy('tanggal_input')->get() as $row)
	@php $no = 1 @endphp
	<tr>
		<td>{{ $no }}</td>
		<td>{{ $row->tingkat_pendidikan->nama }}</td>
		<td>{{ $row->fakultas->nama }}</td>
		<td>{{ $row->jurusan->nama }}</td>
		<td>{{ $row->no_ijazah }}</td>
		<td>{{ format_tanggal_indonesia($row->tanggal_ijazah) }}</td>
		<td>{{ $row->pejabat }}</td>
		<td>{{ $row->nama_sekolah }}</td>
		<td>{{ $row->lokasi }}</td>
		<td>
			<a href="javascript:void" onclick="loadForm({{ $row->id }})"><i class="fa fa-edit"></i> Edit</a>
		</td>
	</tr>
	@php $no++ @endphp
	@empty
	@endforelse
</table>
</div>
<div class="modal fade" id="modal-pendidikan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Form Pendidikan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body body-pendidikan">
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-outline-info proses-button" onclick="doSavePendidikan()" id="simpan">SIMPAN</button>
      </div>
    </div>
  </div>
</div>
@push('styles')
	<link href="{{ asset('vendors/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
	
@endpush
@push('scripts')
<script src="{{ asset('vendors/jquery-ui/jquery-ui.min.js') }}"></script>
	<script>
		function loadForm(id)
		{
			$('.body-pendidikan').html('');
			$.post("{{ URL('riwayat_pendidikan/form') }}/"+id,{pegawai_id:'{{ $pegawai->id }}'}).done(function(resp){
				$('.body-pendidikan').html(resp);
				$('#modal-pendidikan').modal('show')
			}).fail(function(xhr){
				alert(xhr.responseText)
			})
		
		}
		
		function doSavePendidikan()
		{
			var form = $('#form-pendidikan');
			if (form.valid()){
				$.post("{{ URL('riwayat_pendidikan/save') }}",form.serialize()).done(function(resp){
					if (resp == 'success'){
						Toast.fire({
							icon:'success',
							title:"Data Berhasil Disimpan/Diubah dan Menunggu Acc Admin"
						})
						$('#modal-pendidikan').modal('hide')
					} else {
						Toast.fire({
							icon:'error',
							title:resp
						})
					}
				}).fail(function(xhr){
					$('#simpan').prop('disabled',false);
					alert(xhr.responseText);
				})
			}
		}
	</script>
@endpush