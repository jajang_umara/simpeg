@php 
	use App\Models\Mgolongan;
@endphp

<div class="modal fade" id="modal-riwayat-jabatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Riwayat Jabatan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
			<form id="form-riwayat-jabatan">
				<input type="hidden" name="id">
				<input type="hidden" name="pegawai_id" value="{{ @$row->id }}">
				
				
				<div class="form-group row">
					<label class="col-md-3">Nama/NIP Pegawai</label>
					<div class="col-md-9">
						<p class="form-control-static">{{ isset($row->nama) ? $row->nama : @$pegawai->nama }}/{{ isset($row->nip) ? $row->nip : @$pegawai->nip }}</p>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3">Satuan Kerja</label>
					<div class="col-md-9">
						<select class="select2-modal form-control" tabindex="-1" id="satuan_kerja_jabatan" name="satuan_kerja_id" required onchange="getUnitKerja(this.value)" style="width:100%">
							<option value="">Pilih Satuan Kerja</option>
							@foreach(App\Models\MsatuanKerja::where('status',1)->get() as $rowx)
							<option value="{{ $rowx->id }}" @if(isset($row) && $row->satuan_kerja_id == $rowx->id) selected @endif @if(isset($pegawai) && $pegawai->satuan_kerja_id == $rowx->id) selected @endif >{{ $rowx->nama }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3">Unit Kerja</label>
					<div class="col-md-9">
						<select class="form-control select2-modal" name="unit_kerja_id" style="width:100%" id="unit_kerja_jabatan" onchange="selectDefault()">
							<option value="0">-Tanpa Unit Kerja-</option>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3">Jenis Jabatan</label>
					<div class="col-md-6">
						<select class="form-control" name="jenis_jabatan" id="jenis_jabatan_jabatan" onchange="jenisJabatanJabatan(this.value)" >
						@foreach(jenis_jabatan() as $key=>$jab)
						<option value="{{ $key }}" @if(isset($row) && $row->jabatan->jenis == $key) selected @endif @if(isset($pegawai) && $pegawai->jabatan->jenis == $key) selected @endif >{{ $jab }}</option>
						@endforeach
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3">Nama Jabatan</label>
					<div class="col-md-9">
						<select class="form-control" name="jabatan_id" id="jabatan_id_jabatan" >
										
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3">Jabatan TMT</label>
					<div class="col-md-6">
						<input class="form-control datepicker" type="text" name="jabatan_tmt" data-inputmask="'mask':'99-99-9999'" placeholder="dd-mm-yyyy" required value="{{ @tanggal_sql($row->jabatan_tmt) }}">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3">Nomor SK</label>
					<div class="col-md-4">
						<input class="form-control" type="text" name="nomor_sk" required>
					</div>
					<label class="col-md-2">Tanggal SK</label>
					<div class="col-md-3">
						<input class="form-control datepicker" type="text" name="tanggal_sk" data-inputmask="'mask':'99-99-9999'" placeholder="dd-mm-yyyy" required>
					</div>
				</div>
				
				<div class="form-group row">
					<label class="col-md-3">Nama Pejabat</label>
					<div class="col-md-6">
						<input class="form-control" type="text" name="pejabat" required>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3">Lampiran</label>
					<div class="col-md-6">
						<input class="form-control" type="file" name="lampiran" required id="file">
					</div>
				</div>
			</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-outline-info proses-button" onclick="doSaveJabatan()" id="simpan">SIMPAN</button>
      </div>
    </div>
  </div>
</div>
@push('scripts')
<script>
	$(function(){
		$('.select2-modal').select2({
			dropdownParent: $('#modal-riwayat-jabatan')
		});
	})
	var unitKerjaJabatan = {{ isset($row->unit_kerja_id) ? $row->unit_kerja_id : $pegawai->unit_kerja_id }}
	
	getUnitKerja({{ isset($row->satuan_kerja_id) ? $row->satuan_kerja_id : $pegawai->satuan_kerja_id }})
	jenisJabatanJabatan("{{ isset($row) ? $row->jabatan->jenis : $pegawai->jabatan->jenis }}")
		
	function doSaveJabatan()
	{
		$('#form-riwayat-jabatan').validate({
			errorPlacement:function (error, element) {
				error.addClass('invalid-feedback');
				element.closest('div').append(error);
			}
		});
		
		
		
		if($('#form-riwayat-jabatan').valid()){
			var formData = new FormData($('#form-riwayat-jabatan')[0]);
			$.ajax({
				url : "{{ URL('riwayat_jabatan/save') }}",
				type : 'POST',
				data : formData,
				processData: false,  
				contentType: false,  
				success : function(data) {
					if (data == 'success'){
					  $('#modal-riwayat-jabatan').modal('hide');
						Toast.fire({icon:'success',title:"Data Berhasil Disimpan/Diubah,Menunggu Acc Admin"})
				  } else {
					  Toast.fire({icon:'error',title:data})
				  }
				}
			});
		}
	}
	
	function getUnitKerja(value)
	{
		selectDefault();
		var select = $('#unit_kerja_jabatan');
		$.post("{{ URl('unit_kerja/get_unit') }}",{satuan_kerja:value,unit_kerja:unitKerjaJabatan}).done(function(resp){
			select.html(resp)
		})
	}
	
	function jenisJabatanJabatan(value)
	{
		$.post("{{ URL('jabatan/get_jabatan') }}",{jenis:value,satuan_kerja:$('#satuan_kerja_jabatan').val(),unit_kerja:unitKerjaJabatan}).done(function(resp){
			resp = JSON.parse(resp)
			var jabatan = $('#jabatan_id_jabatan');
			jabatan.empty();
			if (resp.length > 0){
				jabatan.append("<option value=''>-Pilih jabatan-</option>")
				$.each(resp,function(k,v){
					var selected = "";
					@if (isset($row))
						if ( {{ $row->jabatan_id}} == v.id){
							selected = 'selected';
						}
					@else
						if ( {{ $pegawai->jabatan_id}} == v.id){
							selected = 'selected';
						}
					@endif
					
					if (value == "STRUKTURAL"){
						jabatan.append("<option value='"+v.id+"' "+selected+">"+v.nama+"</option>")
					} else {
						jabatan.append("<option value='"+v.id+"' "+selected+" >"+v.jf.nama+"</option>")
					}
					
				})
			}
		})
	}
	
	function selectDefault()
	{
		$('#jenis_jabatan_jabatan').val("{{ isset($row) ? $row->jabatan->jenis : $pegawai->jabatan->jenis }}");
	}
</script>
@endpush