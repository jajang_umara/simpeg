@php use App\Models\MriwayatJabatan @endphp
<button class="btn btn-primary float-right" onclick="openformJabatan()"><i class="fa fa-plus"></i> Tambah</button>
<div class="table-responsive">
<table id="tabel-riwayat-pendidikan-nonformal" class="table table-bordered" cellspacing="0" style="width:1000px">
	<thead>
		<tr align="center">
			<th rowspan="2">No.</th>
			<th rowspan="2">Nama Jabatan</th>
			
			
			<th colspan="3">Surat Keputusan</th>
			<th rowspan="2">TMT</th>
			<th rowspan="2">Unit Kerja</th>
		</tr>
		<tr align="center">
			
			<th>Nomor</th>
			<th>Tanggal</th>
			<th>Pejabat</th>
		</tr>
	</thead>
	@forelse(MriwayatJabatan::where('pegawai_id',$pegawai->id)->where('status','!=','DRAFT')->get() as $row)
	@php $no = 1 @endphp
	<tr>
		<td>{{ $no }}</td>
		<td>{{ $row->golongan->nama }}</td>
		
		<td>{{ $row->nomor_sk }}</td>
		<td>{{ format_tanggal_indonesia($row->tanggal_sk) }}</td>
		<td>{{ $row->pejabat }}</td>
		<td>{{ $row->jabatan_tmt }}</td>
		<td>{{ $row->unit_kerja->nama }}</td>
		
	</tr>
	@php $no++ @endphp
	@empty
	@endforelse
</table>
</div>
@include('pages.riwayat.jabatan.form',['pegawai'=>$pegawai])
@push('scripts')
<script>
	function openformJabatan()
	{
		$('#modal-riwayat-jabatan').modal('show');
	}
</script>
@endpush