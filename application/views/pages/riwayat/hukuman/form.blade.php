@php 
	use App\Models\Mhukuman;
@endphp
<form id="form-riwayat-hukuman">
	<input type="hidden" name="id" value="{{ @$id }}">
	<input type="hidden" name="pegawai_id" value="{{ _post('pegawai_id') }}">
	<div class="form-group row">
		<label class="col-md-4">Nama Hukuman</label>
		<div class="col-md-8">
			<select name="hukuman_id" class="form-control">
				<option value="">-Pilih Hukuman-</option>
				@foreach(Mhukuman::orderBy('jenis')->orderBy('nama')->get() as $r)
				<option value="{{ $r->id }}"  @if(isset($row) && $row->hukuman_id == $r->id) selected @endif >{{ $r->nama }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Tanggal Mulai</label>
		<div class="col-md-8">
			<input type="text" name="tanggal_mulai" class="form-control datepicker" required value="{{ @tanggal_sql($row->tanggal_mulai) }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Tanggal Selesai</label>
		<div class="col-md-8">
			<input type="text" name="tanggal_selesai" class="form-control datepicker" required value="{{ @tanggal_sql($row->tanggal_selesai) }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Nomor SK</label>
		<div class="col-md-8">
			<input type="text" name="nomor_sk" class="form-control" required value="{{ @$row->nomor_sk }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Tanggal SK</label>
		<div class="col-md-8">
			<input type="text" name="tanggal_sk" class="form-control datepicker" required value="{{ @tanggal_sql($row->tanggal_sk) }}">
		</div>
	</div>
	
	<div class="form-group row">
		<label class="col-md-4">Keterangan</label>
		<div class="col-md-8">
			<input type="text" name="keterangan" class="form-control" required value="{{ @$row->keterangan }}">
		</div>
	</div>
	
</form>

<script>
	
	
	$('.datepicker').daterangepicker({
		singleDatePicker:true,
		locale: {
			format:"DD-MM-YYYY",
		}
	})
	
	$('#form-riwayat-hukuman').validate({
		errorPlacement:function (error, element) {
			error.addClass('invalid-feedback');
			element.closest('div').append(error);
		}
	});
	
</script>


