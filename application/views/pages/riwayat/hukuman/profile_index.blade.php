@php use App\Models\MriwayatHukuman @endphp
<button class="btn btn-primary float-right" onclick="openHukuman(0)"><i class="fa fa-plus"></i> Tambah</button>
<div class="table-responsive">
<table id="tabel-riwayat-hukuman" class="table table-bordered" cellspacing="0" style="width:1000px">
	<thead>
		<tr align="center">
			<th rowspan="2">No.</th>
			<th rowspan="2">Nama Hukuman</th>
			<th rowspan="2">Kategori</th>
			<th colspan="2">Lama</th>
			<th colspan="2">SK</th>
			<th rowspan="2">Keterangan Hukuman</th>
			<th rowspan="2">Pilihan</th>
		</tr>
		<tr align="center">
			<th>Mulai</th>
			<th>selesai</th>
			<th>Nomor</th>
			<th>Tanggal</th>
			
		</tr>
	</thead>
	@forelse(MriwayatHukuman::where('pegawai_id',$pegawai->id)->where('jenis','DATA')->orderBy('tanggal_input')->get() as $row)
	@php $no = 1 @endphp
	<tr>
		<td>{{ $no }}</td>
		<td>{{ $row->hukuman->nama }}</td>
		<td>{{ $row->hukuman->jenis }}</td>
		<td>{{ format_tanggal_indonesia($row->tanggal_mulai) }}</td>
		<td>{{ format_tanggal_indonesia($row->tanggal_selesai) }}</td>
		<td>{{ $row->nomor_sk }}</td>
		<td>{{ format_tanggal_indonesia($row->tanggal_sk) }}</td>
		<td>{{ $row->keterangan }}</td>
		<td>
			<a href="javascript:void" onclick="openHukuman({{ $row->id }})"><i class="fa fa-edit"></i> Edit</a>
		</td>
	</tr>
	@php $no++ @endphp
	@empty
	@endforelse
</table>
</div>
<div class="modal fade" id="modal-riwayat-hukuman" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Form Hukuman</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body body-hukuman">
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-outline-info proses-button" onclick="doSaveHukuman()" id="simpan">SIMPAN</button>
      </div>
    </div>
  </div>
</div>

@push('scripts')

	<script>
		function openHukuman(id)
		{
			$('.body-hukuman').html('');
			$.post("{{ URL('riwayat_hukuman/form') }}/"+id,{pegawai_id:'{{ $pegawai->id }}'}).done(function(resp){
				$('.body-hukuman').html(resp);
				$('#modal-riwayat-hukuman').modal('show')
			}).fail(function(xhr){
				alert(xhr.responseText)
			})
		
		}
		
		function doSaveHukuman()
		{
			var form = $('#form-riwayat-hukuman');
			if (form.valid()){
				$.post("{{ URL('riwayat_hukuman/save') }}",form.serialize()).done(function(resp){
					if (resp == 'success'){
						Toast.fire({
							icon:'success',
							title:"Data Berhasil Disimpan/Diubah dan Menunggu Acc Admin"
						})
						$('#modal-riwayat-hukuman').modal('hide')
					} else {
						Toast.fire({
							icon:'error',
							title:resp
						})
					}
				}).fail(function(xhr){
					
					alert(xhr.responseText);
				})
			}
		}
	</script>
@endpush