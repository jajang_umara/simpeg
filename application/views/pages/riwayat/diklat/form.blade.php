
<form id="form-diklat">
	<input type="hidden" name="id" value="{{ @$id }}">
	<input type="hidden" name="pegawai_id" value="{{ _post('pegawai_id') }}">
	<input type="hidden" name="jenis_diklat" value="{{ _post('jenis') }}">
	@if(_post('jenis') == 'STRUKTURAL')
	<div class="form-group row">
		<label class="col-md-4">Nama Diklat</label>
		<div class="col-md-8">
			<select name="struktural_id" class="form-control" required>
				<option value="">-Pilih Diklat-</option>
				@foreach(App\Models\MdiklatStruktural::orderBy('nama')->get() as $r)
				<option value="{{$r->id}}">{{ $r->nama }}</option>
				@endforeach
			</select>
		</div>
	</div>
	@endif
	@if(_post('jenis') == 'FUNGSIONAL')
	<div class="form-group row">
		<label class="col-md-4">Nama Diklat</label>
		<div class="col-md-8">
			<select name="fungsional_id" class="form-control" required id="fungsional_id" style="width:100%">
				<option value="">-Pilih Diklat-</option>
				@foreach(App\Models\MdiklatFungsional::orderBy('nama')->get() as $r)
				<option value="{{$r->id}}">{{ $r->nama }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<script>
		$('#fungsional_id').select2()
	</script>
	@endif
		@if(_post('jenis') == 'TEKNIS')
	<div class="form-group row">
		<label class="col-md-4">Nama Diklat</label>
		<div class="col-md-8">
			<select name="teknis_id" class="form-control" required id="teknis_id" style="width:100%">
				<option value="">-Pilih Diklat-</option>
				@foreach(App\Models\MdiklatTeknis::orderBy('nama')->get() as $r)
				<option value="{{$r->id}}">{{ $r->nama }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<script>
		$('#teknis_id').select2()
	</script>
	@endif
	<div class="form-group row">
		<label class="col-md-4">Tanggal Mulai</label>
		<div class="col-md-8">
			<input type="text" name="tanggal_mulai" class="form-control datepicker" required value="{{ @tanggal_sql($row->tanggal_mulai) }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Tanggal Selesai</label>
		<div class="col-md-8">
			<input type="text" name="tanggal_selesai" class="form-control datepicker" required value="{{ @tanggal_sql($row->tanggal_selesai) }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Jumlah Jam</label>
		<div class="col-md-8">
			<input type="text" name="jumlah_jam" class="form-control" required value="{{ @$row->jumlah_jam }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Nomor STTP</label>
		<div class="col-md-8">
			<input type="text" name="sttp_no" class="form-control" required value="{{ @$row->sttp_no }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Tanggal STTP</label>
		<div class="col-md-8">
			<input type="text" name="sttp_tanggal" class="form-control datepicker" required value="{{ @tanggal_sql($row->sttp_tanggal) }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Nama Pejabat</label>
		<div class="col-md-8">
			<input type="text" name="sttp_pejabat" class="form-control" required value="{{ @$row->sttp_pejabat }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Instansi Penyelenggara</label>
		<div class="col-md-8">
			<input type="text" name="penyelenggara" class="form-control" required value="{{ @$row->penyelenggara }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Tempat/Lokasi</label>
		<div class="col-md-8">
			<input type="text" name="tempat" class="form-control" required value="{{ @$row->tempat }}">
		</div>
	</div>
</form>

<script>
	
	
	$('.datepicker').daterangepicker({
		singleDatePicker:true,
		locale: {
			format:"DD-MM-YYYY",
		}
	})
	$(function() {
		$('#form-diklat').validate({
			errorPlacement:function (error, element) {
				error.addClass('invalid-feedback');
				element.closest('div').append(error);
			}
		});
	})
	
</script>

<style>
	ul.ui-autocomplete {
    z-index: 1100;
}
</style>
