@php use App\Models\MriwayatDiklat @endphp
<button class="btn btn-primary float-right" onclick="openDiklat(0,'{{ $jenis }}')"><i class="fa fa-plus"></i> Tambah</button>
<div class="table-responsive">
<table id="tabel-riwayat-pendidikan-nonformal" class="table table-bordered" cellspacing="0" style="width:1000px">
	<thead>
		<tr align="center">
			<th rowspan="2">No.</th>
			<th rowspan="2">Nama Diklat</th>
			<th colspan="2">Tanggal</th>
			<th colspan="3"><center>STTP</center></th>
			<th colspan="2">Instansi Penyelenggara</th>
			<th rowspan="2">Pilihan</th>
		</tr>
		<tr align="center">
			<th>Mulai</th>
			<th>selesai</th>
			<th>Nomor</th>
			<th>Tanggal</th>
			<th>Nama Pejabat</th>
			<th>Instansi</th>
			<th>Tempat</th>
		</tr>
	</thead>
	@forelse(MriwayatDiklat::where('pegawai_id',$pegawai->id)->where('jenis','DATA')->where('jenis_diklat',$jenis)->orderBy('tanggal_input')->get() as $row)
	@php $no = 1 @endphp
	<tr>
		<td>{{ $no }}</td>
		<td>{{ @$row->struktural->nama }}</td>
		<td>{{ format_tanggal_indonesia($row->tanggal_mulai) }}</td>
		<td>{{ format_tanggal_indonesia($row->tanggal_selesai) }}</td>
		<td>{{ $row->jumlah_jam }}</td>
		<td>{{ $row->sttp_no }}</td>
		<td>{{ format_tanggal_indonesia($row->sttp_tanggal) }}</td>
		<td>{{ $row->sttp_pejabat }}</td>
		<td>{{ $row->penyelenggara }}</td>
		<td>{{ $row->tempat }}</td>
		<td>
			<a href="javascript:void" onclick="openDiklat({{ $row->id }},'{{ $jenis }}')"><i class="fa fa-edit"></i> Edit</a>
		</td>
	</tr>
	@php $no++ @endphp
	@empty
	@endforelse
</table>
</div>


