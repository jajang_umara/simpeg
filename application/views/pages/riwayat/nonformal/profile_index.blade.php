@php use App\Models\MriwayatNonformal @endphp
<button class="btn btn-primary float-right" onclick="openNonFormal(0)"><i class="fa fa-plus"></i> Tambah</button>
<div class="table-responsive">
<table id="tabel-riwayat-pendidikan-nonformal" class="table table-bordered" cellspacing="0" style="width:1000px">
	<thead>
		<tr align="center">
			<th rowspan="2">No.</th>
			<th rowspan="2">Nama Kursus/Seminar/LokaKarya</th>
			<th colspan="2">Tanggal</th>
			<th colspan="3">Ijazah/Tanda Lulus/Surat Keterangan</th>
			<th rowspan="2">Instansi<br/>Penyelenggara</th>
			<th rowspan="2">Tempat</th>
			<th rowspan="2">Pilihan</th>
		</tr>
		<tr align="center">
			<th>Mulai</th>
			<th>selesai</th>
			<th>Nomor</th>
			<th>Tanggal</th>
			<th>Nama Pejabat</th>
		</tr>
	</thead>
	@forelse(MriwayatNonformal::where('pegawai_id',$pegawai->id)->where('jenis','DATA')->orderBy('tanggal_input')->get() as $row)
	@php $no = 1 @endphp
	<tr>
		<td>{{ $no }}</td>
		<td>{{ $row->nama }}</td>
		<td>{{ format_tanggal_indonesia($row->tanggal_mulai) }}</td>
		<td>{{ format_tanggal_indonesia($row->tanggal_selesai) }}</td>
		<td>{{ $row->sttp }}</td>
		<td>{{ format_tanggal_indonesia($row->sttp_tanggal) }}</td>
		<td>{{ $row->sttp_pejabat }}</td>
		<td>{{ $row->penyelenggara }}</td>
		<td>{{ $row->tempat }}</td>
		<td>
			<a href="javascript:void" onclick="openNonFormal({{ $row->id }})"><i class="fa fa-edit"></i> Edit</a>
		</td>
	</tr>
	@php $no++ @endphp
	@empty
	@endforelse
</table>
</div>
<div class="modal fade" id="modal-pendidikan-nonformal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Form Pendidikan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body body-pendidikan-nonformal">
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-outline-info proses-button" onclick="doSaveNonFormal()" id="simpan">SIMPAN</button>
      </div>
    </div>
  </div>
</div>
@push('styles')
	<link href="{{ asset('vendors/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
	
@endpush
@push('scripts')
<script src="{{ asset('vendors/jquery-ui/jquery-ui.min.js') }}"></script>
	<script>
		function openNonFormal(id)
		{
			$('.body-pendidikan-nonformal').html('');
			$.post("{{ URL('riwayat_nonformal/form') }}/"+id,{pegawai_id:'{{ $pegawai->id }}'}).done(function(resp){
				$('.body-pendidikan-nonformal').html(resp);
				$('#modal-pendidikan-nonformal').modal('show')
			}).fail(function(xhr){
				alert(xhr.responseText)
			})
		
		}
		
		function doSaveNonFormal()
		{
			var form = $('#form-pendidikan-nonformal');
			if (form.valid()){
				$.post("{{ URL('riwayat_nonformal/save') }}",form.serialize()).done(function(resp){
					if (resp == 'success'){
						Toast.fire({
							icon:'success',
							title:"Data Berhasil Disimpan/Diubah dan Menunggu Acc Admin"
						})
						$('#modal-pendidikan-nonformal').modal('hide')
					} else {
						Toast.fire({
							icon:'error',
							title:resp
						})
					}
				}).fail(function(xhr){
					
					alert(xhr.responseText);
				})
			}
		}
	</script>
@endpush