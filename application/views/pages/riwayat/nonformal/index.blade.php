@extends('layouts.main')
@push('styles')
	<link href="{{ asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@endpush
@section('content')
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Draft Riwayat Pendidikan Non formal<small></small></h3>
			</div>
		</div>
		<div class="clearfix"></div>
		
		<div class="row">
			<div class="col-md-12 col-sm-12 ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Draft Riwayat Pendidikan Non Formal</h2>
							<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="row">
								<div class="col-sm-12">
									<div class="card-box table-responsive">
										
										<table id="myTable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" style="width:100%">
											<thead>
												<tr>
													<th>Aksi</th>
													<th>Nama Pegawai</th>
													<th>Nama kursus</th>
													<th>Tanggal Mulai/Selesai</th>
													<th>No. Kursus</th>
													<th>Tanggal Kursus</th>
													<th>Nama Pejabat</th>
													<th>Status</th>
													<th>Keterangan</th>
													<th>Tanggal Input</th>
												</tr>
											</thead>
												
										</table>
									</div>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
							
	</div>
@endsection



@push('scripts')
	<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script>
		$(function(){
			table = $('#myTable').DataTable({
				"processing": true,
				"serverSide": true,
				"ajax": {
					"url": "{{ URL('riwayat_nonformal/data') }}",
					"type":"POST",
					"data": function ( d ) {
					}
				},
				columns:[
					{ "data": "aksi","orderable":false,"render":function(data,type,row){
						
						var button = '<button onclick="accData('+row.id+')" class="btn btn-outline-warning btn-sm" data-toggle="tooltip" data-placement="top" title="ACC"><i class="fa fa-check"></i></button>'
						if (row.status_draft == 'ACC'){
							return "";
						}	
						return button;
					}},
					{ "data": "nama_pegawai" },
					{ "data": "nama" },
					{ "data": "tanggal_mulai","render":function(data,type,row){
						return data+" s/d "+row.tanggal_selesai
					}},
					{"data":"sttp"},
					{"data":"sttp_tanggal"},
					{"data":"sttp_pejabat"},
					{"data":"status_draft"},
					{"data":"keterangan_draft"},
					{"data":"tanggal_input"}
				],
				"order": [[7, 'desc']],
			});
		});
		
		
		
		function accData(id)
		{
			Swal.fire({
				title: "Apakah Yakin akan Acc data ini ?",
				icon: "warning",
				showCancelButton: true,
				confirmButtonText: 'Ya',
			}).then(function(isConfirm){
				if (isConfirm.value) {
					$.post("{{ site_url('riwayat_nonformal/acc') }}",{id:id}).done(function(result){
						if (result == 'success') {
							table.draw(false);
							Toast.fire({icon:'success',title:"Data Berhasil Dihapus"})
							
						} else {
							Toast.fire({icon:'error',title:result})
						}
					}).fail(function(xhr){
						Toast.fire({icon:'error',title:xhr.responseText})
					})
				}
			});
		}
	</script>
@endpush