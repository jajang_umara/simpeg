@php 
	use App\Models\MtingkatPendidikan; 
	use App\Models\Mjurusan;
	use App\Models\Mfakultas;
@endphp
<form id="form-pendidikan-nonformal">
	<input type="hidden" name="id" value="{{ @$id }}">
	<input type="hidden" name="pegawai_id" value="{{ _post('pegawai_id') }}">
	<div class="form-group row">
		<label class="col-md-4">Nama Kursus/Seminar/Lokakarya</label>
		<div class="col-md-8">
			<input type="text" name="nama" class="form-control"  required value="{{ @$row->nama }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Tanggal Mulai</label>
		<div class="col-md-8">
			<input type="text" name="tanggal_mulai" class="form-control datepicker" required value="{{ @tanggal_sql($row->tanggal_mulai) }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Tanggal Selesai</label>
		<div class="col-md-8">
			<input type="text" name="tanggal_selesai" class="form-control datepicker" required value="{{ @tanggal_sql($row->tanggal_selesai) }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Nomor Ijazah/Tanda Lulus</label>
		<div class="col-md-8">
			<input type="text" name="sttp" class="form-control" required value="{{ @$row->sttp }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Tanggal Ijazah/Tanda Lulus</label>
		<div class="col-md-8">
			<input type="text" name="sttp_tanggal" class="form-control datepicker" required value="{{ @tanggal_sql($row->sttp_tanggal) }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Nama Pejabat</label>
		<div class="col-md-8">
			<input type="text" name="sttp_pejabat" class="form-control" required value="{{ @$row->sttp_pejabat }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Instansi Penyelenggara</label>
		<div class="col-md-8">
			<input type="text" name="penyelenggara" class="form-control" required value="{{ @$row->penyelenggara }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Tempat/Lokasi</label>
		<div class="col-md-8">
			<input type="text" name="tempat" class="form-control" required value="{{ @$row->tempat }}">
		</div>
	</div>
</form>

<script>
	function tingkatChange(){
		if (parseInt($('#tingkat_pendidikan_id option:selected').data('urutan')) > 3){
			$( "#nama_sekolah" ).autocomplete({
				source: "{{ URL('riwayat_pendidikan/universitas') }}",
				minLength: 4,
			})
			$('[name="fakultas_id"]').prop('disabled',false);
		} else {
			$('[name="fakultas_id"]').prop('disabled',true);
		}
	}
	
	$('.datepicker').daterangepicker({
		singleDatePicker:true,
		locale: {
			format:"DD-MM-YYYY",
		}
	})
	
	$('#form-pendidikan').validate({
		errorPlacement:function (error, element) {
			error.addClass('invalid-feedback');
			element.closest('div').append(error);
		}
	});
	
</script>

<style>
	ul.ui-autocomplete {
    z-index: 1100;
}
</style>
