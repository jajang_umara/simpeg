@php 
	use App\Models\Mgolongan;
@endphp

<div class="modal fade" id="modal-riwayat-pangkat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Riwayat Pangkat</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
			<form id="form-riwayat-pangkat">
				<input type="hidden" name="id" value="{{ @$id }}">
				<input type="hidden" name="pegawai_id" value="{{ isset($row->id) ? $row->id : $pegawai->id }}">
				<input type="hidden" name="satuan_kerja_id" value="{{ isset($row->satuan_kerja_id) ? $row->satuan_kerja_id : $pegawai->satuan_kerja_id }}">
				<input type="hidden" name="unit_kerja_id" value="{{ isset($row->unit_kerja_id) ? $row->unit_kerja_id : $pegawai->unit_kerja_id }}">
				<div class="form-group row">
					<label class="col-md-3">Satuan Kerja</label>
					<div class="col-md-9">
						<p class="form-control-static">{{ isset($row->satuan_kerja->nama) ? $row->satuan_kerja->nama : $pegawai->satuan_kerja->nama }}</p>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3">Unit Kerja</label>
					<div class="col-md-9">
						<p class="form-control-static">{{ isset($row->unit_kerja->nama) ? $row->unit_kerja->nama : $pegawai->unit_kerja->nama  }}</p>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3">Nama/NIP Pegawai</label>
					<div class="col-md-9">
						<p class="form-control-static">{{ isset($row->nama) ? $row->nama : $pegawai->nama  }}/{{ isset($row->nip) ? $row->nip : $pegawai->nip }}</p>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3">Golongan Baru</label>
					<div class="col-md-6">
						<select class="form-control" name="golongan_id" required>
							<option value="">-Pilih Golongan-</option>
							@foreach(Mgolongan::all() as $rowx)
							<option value="{{ $rowx->id }}" @if(isset($row) && $row->gol_akhir_id == $rowx->id) selected @endif>{{ $rowx->nama }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3">Golongan Baru TMT</label>
					<div class="col-md-6">
						<input class="form-control datepicker" type="text" name="golongan_tmt" data-inputmask="'mask':'99-99-9999'" placeholder="dd-mm-yyyy" required value="{{ @tanggal_sql($row->gol_akhir_tmt) }}">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3">Nomor SK</label>
					<div class="col-md-4">
						<input class="form-control" type="text" name="nomor_sk" required>
					</div>
					<label class="col-md-2">Tanggal SK</label>
					<div class="col-md-3">
						<input class="form-control datepicker" type="text" name="tanggal_sk" data-inputmask="'mask':'99-99-9999'" placeholder="dd-mm-yyyy" required>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3">Tahun Kerja</label>
					<div class="col-md-2">
						<input class="form-control" type="text" name="pangkat_tahun" value="{{ @$row->kerja_tahun }}" required>
					</div>
					<label class="col-md-2">Bulan Kerja</label>
					<div class="col-md-2">
						<input class="form-control" type="text" name="pangkat_bulan" value="{{ @$row->kerja_bulan }}" required>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3">Nama Pejabat</label>
					<div class="col-md-6">
						<input class="form-control" type="text" name="pangkat_pejabat" required>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3">Lampiran</label>
					<div class="col-md-6">
						<input class="form-control" type="file" name="lampiran" required id="file">
					</div>
				</div>
			</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-outline-info proses-button" onclick="doSave()" id="simpan">SIMPAN</button>
      </div>
    </div>
  </div>
</div>

<script>
	function doSave()
	{
		$('#form-riwayat-pangkat').validate({
			errorPlacement:function (error, element) {
				error.addClass('invalid-feedback');
				element.closest('div').append(error);
			}
		});
		if($('#form-riwayat-pangkat').valid()){
			var formData = new FormData($('#form-riwayat-pangkat')[0]);
			$.ajax({
				url : "{{ URL('riwayat_pangkat/save') }}",
				type : 'POST',
				data : formData,
				processData: false,  
				contentType: false,  
				success : function(data) {
					if (data == 'success'){
					  $('#modal-riwayat-pangkat').modal('hide');
						Toast.fire({icon:'success',title:"Data Berhasil Disimpan/Diubah"})
				  } else {
					  Toast.fire({icon:'error',title:data})
				  }
				}
			});
		}
	}
</script>