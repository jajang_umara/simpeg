@extends('layouts.main')
@push('styles')
	<link href="{{ asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@endpush
@section('content')
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Riwayat Pangkat <small></small></h3>
			</div>
		</div>
		<div class="clearfix"></div>
		
		<div class="row">
			<div class="col-md-12 col-sm-12 ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Riwayat Pangkat</h2>
						<ul class="nav navbar-right panel_toolbox">
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="row">
								<div class="col-sm-12">
									<div class="card-box table-responsive">
										
										<table id="myTable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" style="width:100%">
											<thead>
												<tr>
													<th>Aksi</th>
													<th>Satuan Kerja</th>
													<th>Unit Kerja</th>
													<th>Nama</th>
													<th>Nama Golongan</th>
													<th>Golongan TMT</th>
													<th>Lampiran</th>
													<th>Status</th>
													<th>Keterangan</th>
												</tr>
											</thead>
												
										</table>
									</div>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
							
	</div>
@endsection

@section('modal')
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-outline-info proses-button" onclick="doSave()" id="simpan">SIMPAN</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
	<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script>
		$(function(){
			table = $('#myTable').DataTable({
				"processing": true,
				"serverSide": true,
				"ajax": {
					"url": "{{ URL('riwayat_pangkat/data') }}",
					"type":"POST",
					"data": function ( d ) {
						d.status = "{{ @$status }}"
					}
				},
				columns:[
					{ "data": "aksi","orderable":false,"render":function(data,type,row){
						var button = '<button onclick="accData('+row.id+')" class="btn btn-outline-info btn-sm" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-check"></i> Acc</button>&nbsp;'; 
						   
							return button;
					}},
					{ "data": "nama_satuan_kerja"},
					{ "data": "nama_unit_kerja"},
					{ "data": "nama","render":function(data,type,row){
						return data+" ( "+row.nip+" )"
					}},
					{ "data": "nama_golongan"},
					{ "data": "golongan_tmt"},
					{ "data": "lampiran","render":function(data){
						return "<a href='{{ base_url('uploads/lampiran/riwayat_pangkat') }}/"+data+"' target='_blank'>Lampiran</a>";
					}},
					{ "data": "status"},
					{ "data": "keterangan"},
				],
				"order": [[5, 'desc']],
			});
		});
		
		
		
		
		
		function accData(id)
		{
			Swal.fire({
				title: "Apakah Yakin akan Acc data ini ?",
				icon: "warning",
				showCancelButton: true,
				confirmButtonText: 'Ya',
			}).then(function(isConfirm){
				if (isConfirm.value) {
					$.post("{{ URL('riwayat_pangkat/acc_data') }}",{id:id}).done(function(result){
						if (result == 'success') {
							table.draw(false);
							Toast.fire({icon:'success',title:"Data Berhasil Diacc"})
							
						} else {
							Toast.fire({icon:'error',title:result})
						}
					}).fail(function(xhr){
						Toast.fire({icon:'error',title:xhr.responseText})
					})
				}
			});
		}
	</script>
@endpush