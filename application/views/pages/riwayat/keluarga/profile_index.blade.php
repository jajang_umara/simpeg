@php use App\Models\MriwayatKeluarga @endphp
<button class="btn btn-primary float-right" onclick="openKeluarga(0,'{{ $jenis }}')"><i class="fa fa-plus"></i> Tambah</button>
<div class="table-responsive">
<table id="tabel-riwayat-pendidikan-nonformal" class="table table-bordered" cellspacing="0" style="width:1000px">
	<thead>
		<tr align="center">
			<th>No</th>
			<th>NIK</th>
			<th>Nama {{ jenis_keluarga($jenis) }}</th>
			<th>Tempat dan Tanggal Lahir</th>
			@if($jenis == 1)
			<th>Tanggal Nikah</th>
			@endif
			@if(in_array($jenis,[2,3,4]))
			<th>Jenis Kelamin</th>
			@endif
			@if($jenis == 2)
			<th>Status Perkawinan</th>
			@endif
			@if(in_array($jenis,[1,2]))
			<th>Memperoleh Tunjangan</th>
			@endif
			<th>Pendidikan</th>
			<th>Pekerjaan</th>
			<th>Keterangan</th>
			<th>Aksi</th>
		</tr>
	</thead>
	@forelse(MriwayatKeluarga::where('pegawai_id',$pegawai->id)->where('jenis','DATA')->where('hubungan',$jenis)->orderBy('tanggal_input')->get() as $row)
	@php $no = 1 @endphp
	<tr>
		<td>{{ $no }}</td>
		<td>{{ @$row->nik }}</td>
		<td>{{ @$row->nama }}</td>
		<td>{{ $row->tempat_lahir }},{{ format_tanggal_indonesia($row->tanggal_lahir) }}</td>
		@if($jenis == 1)
		<td>{{ format_tanggal_indonesia($row->tanggal_nikah) }}</td>
		@endif
		@if(in_array($jenis,[2,3,4]))
		<td>{{ $row->jenis_kelamin }}</td>
		@endif
		@if($jenis == 2)
		<td>{{ $row->status_perkawinan }}</td>
		@endif
		@if(in_array($jenis,[1,2]))
		<td>{{ $row->status_tunjangan == 1 ? 'Ya' : 'Tidak' }}</td>
		@endif
		<td>{{ $row->pendidikan }}</td>
		<td>{{ $row->pekerjaan }}</td>
		<td>{{ $row->keterangan_keluarga }}</td>
		
		<td>
			<a href="javascript:void" onclick="openKeluarga({{ $row->id }},'{{ $jenis }}')"><i class="fa fa-edit"></i> Edit</a>
		</td>
	</tr>
	@php $no++ @endphp
	@empty
	@endforelse
</table>
</div>


