
<form id="form-keluarga">
	<input type="hidden" name="id" value="{{ @$id }}">
	<input type="hidden" name="pegawai_id" value="{{ _post('pegawai_id') }}">
	<input type="hidden" name="hubungan" value="{{ _post('jenis') }}">
	<div class="form-group row">
		<label class="col-md-4">NIK</label>
		<div class="col-md-8">
			<input type="text" name="nik" class="form-control" required value="{{ @$row->nik }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Nama {{ jenis_keluarga(_post('jenis')) }}</label>
		<div class="col-md-8">
			<input type="text" name="nama" class="form-control" required value="{{ @$row->nama }}">
		</div>
	</div>
	@if(in_array(_post('jenis'),[2,3,4]))
	<div class="form-group row">
		<label class="col-md-4">Jenis Kelamin</label>
		<div class="col-md-8">
		@foreach(['Laki-laki','Perempuan'] as $jk)
			<div class="form-check form-check-inline">
			  <input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio1" value="{{ $jk }}" @if(isset($row) && $row->jenis_kelamin == $jk) checked @endif >
			  <label class="form-check-label" for="inlineRadio1">{{ $jk }}</label>
			</div>
		@endforeach	
		</div>
	</div>
	@endif
	<div class="form-group row">
		<label class="col-md-4">Tempat Lahir</label>
		<div class="col-md-8">
			<input type="text" name="tempat_lahir" class="form-control" required value="{{ @$row->tempat_lahir }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Tanggal Lahir</label>
		<div class="col-md-8">
			<input type="text" name="tanggal_lahir" class="form-control datepicker" required value="{{ @tanggal_sql($row->tanggal_lahir) }}">
		</div>
	</div>
	@if(in_array(_post('jenis'),[2]))
	<div class="form-group row">
		<label class="col-md-4">Status Perkawinan</label>
		<div class="col-md-8">
		@foreach(['Kawin','Belum Kawin','Janda','Duda'] as $sp)
			<div class="form-check form-check-inline">
			  <input class="form-check-input" type="radio" name="status_perkawinan" id="inlineRadio1" value="{{ $sp }}" @if(isset($row) && $row->status_perkawinan == $sp) checked @endif >
			  <label class="form-check-label" for="inlineRadio1">{{ $sp }}</label>
			</div>
		@endforeach	
		</div>
	</div>
	@endif
	@if(_post('jenis') == 1)
	<div class="form-group row">
		<label class="col-md-4">Tanggal Menikah</label>
		<div class="col-md-8">
			<input type="text" name="tanggal_nikah" class="form-control datepicker" required value="{{ @tanggal_sql($row->tanggal_nikah) }}">
		</div>
	</div>
	@endif
	@if(in_array(_post('jenis'),[1,2]))
	<div class="form-group row">
		<label class="col-md-4">Memperoleh Tunjangan</label>
		<div class="col-md-8">
		@foreach(['1'=>'Ya','0'=>'Tidak'] as $key=>$tunj)
			<div class="form-check form-check-inline">
			  <input class="form-check-input" type="radio" name="status_tunjangan" id="inlineRadio1" value="{{ $key }}" @if(isset($row) && $row->status_tunjangan == $key) checked @endif >
			  <label class="form-check-label" for="inlineRadio1">{{ $tunj }}</label>
			</div>
		@endforeach	
		</div>
	</div>
	@endif
	<div class="form-group row">
		<label class="col-md-4">Pendidikan</label>
		<div class="col-md-8">
			<input type="text" name="pendidikan" class="form-control" required value="{{ @$row->pendidikan }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Pekerjaan</label>
		<div class="col-md-8">
			<input type="text" name="pekerjaan" class="form-control" required value="{{ @$row->pekerjaan }}">
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-4">Keterangan</label>
		<div class="col-md-8">
			<input type="text" name="keterangan_keluarga" class="form-control" required value="{{ @$row->keterangan_keluarga }}">
		</div>
	</div>
</form>

<script>
	
	
	$('.datepicker').daterangepicker({
		singleDatePicker:true,
		locale: {
			format:"DD-MM-YYYY",
		}
	})
	$(function() {
		$('#form-keluarga').validate({
			errorPlacement:function (error, element) {
				error.addClass('invalid-feedback');
				element.closest('div').append(error);
			}
		});
	})
	
</script>


