@extends('layouts.main')
@section('content')
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Data pegawai<small></small></h3>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Data Pegawai</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<form method="get" id="form-satker">
							<div class="form-group row">
								<label class="control-label col-md-3 col-sm-3 ">Pilih Satuan Kerja</label>
								<div class="col-md-6 col-sm-6 ">
									<select class="select2 form-control" tabindex="-1" name="satuan_kerja" required onchange="$('#form-satker').submit()">
										<option value="">Pilih Satuan Kerja</option>
										@foreach(App\Models\MsatuanKerja::where('status',1)->get() as $row)
										<option value="{{ $row->id }}" @if(_get('satuan_kerja') == $row->id) selected @endif >{{ $row->nama }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</form>
						
						@if(_get('satuan_kerja') != "")
							@include('pages.pegawai.list_pegawai')
						@endif
						
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection



