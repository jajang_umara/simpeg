@extends('layouts.main')
@section('content')
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Detail pegawai<small></small></h3>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Detail Pegawai</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
					 <div class="col-md-3 col-sm-3  profile_left">
							<div class="profile_img">
								<div id="crop-avatar">
									<!-- Current avatar -->
									<img class="img-responsive avatar-view" src="{{ base_url('uploads/photo/'.$pegawai->photo) }}" alt="Avatar" title="Change the avatar" width="200">
								</div>
							</div>
							<h3>{{ $pegawai->nama }}</h3>
							 <ul class="list-unstyled user_data">
								<li><i class="fa fa-map-marker user-profile-icon"></i> SOPD : {{ $pegawai->satuan_kerja->nama }}
								</li>

								<li>
									<i class="fa fa-briefcase user-profile-icon"></i> NIP : {{ $pegawai->nip }}
								</li>

								
							</ul>
							 <a class="btn btn-success btn-sm"><i class="fa fa-edit m-right-xs"></i>Ubah</a>
							 <div class="btn-group">
							  <button type="button" class="btn btn-danger dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								 <i class="fa fa-exclamation-triangle m-right-xs"></i> Ubah Status Kepegawaian
							  </button>
							  <div class="dropdown-menu">
								 <a class="dropdown-item" href="#">Set Pensiun</a>
								 <a class="dropdown-item" href="#">Set Meninggal Dunia</a>
								 <a class="dropdown-item" href="#">Set Pindah Ke luar Pemkab</a>
								 <a class="dropdown-item" href="#">Set CTLN / Tugas Belajar</a>
							  </div>
							</div>
                      <br />
						</div>
						 <div class="col-md-9 col-sm-9 ">
								
								<ul class="nav nav-tabs" id="myTab" role="tablist">
									<li class="nav-item">
										<a class="nav-link active" id="home-tab" data-toggle="tab" href="#data-pribadi" role="tab" aria-controls="home" aria-selected="true">Data Pribadi</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="profile-tab" data-toggle="tab" href="#pendidikan-formal" role="tab" aria-controls="profile" aria-selected="true">Riwayat Pendidikan Formal</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="contact-tab" data-toggle="tab" href="#non-formal" role="tab" aria-controls="contact" aria-selected="false">Riwayat Pendidikan Non Formal</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="pangkat-tab" data-toggle="tab" href="#riwayat-kepangkatan" role="tab" aria-controls="riwayat-kepangkatan" aria-selected="false">Riwayat Kepangkatan</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="jabatan-tab" data-toggle="tab" href="#riwayat-jabatan" role="tab" aria-controls="riwayat-jabatan" aria-selected="false">Riwayat Jabatan</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="hukuman-tab" data-toggle="tab" href="#riwayat-hukuman" role="tab" aria-controls="riwayat-hukuman" aria-selected="false">Riwayat HUKDIS</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="diklat-struktural-tab" data-toggle="tab" href="#diklat-struktural" role="tab" aria-controls="diklat-struktural" aria-selected="false">Riwayat Diklat Struktural</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="diklat-fungsional-tab" data-toggle="tab" href="#diklat-fungsional" role="tab" aria-controls="diklat-fungsional" aria-selected="false">Riwayat Diklat Fungsional</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="diklat-teknis-tab" data-toggle="tab" href="#diklat-teknis" role="tab" aria-controls="diklat-teknis" aria-selected="false">Riwayat Diklat Teknis</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="istri-tab" data-toggle="tab" href="#riwayat-istri" role="tab" aria-controls="riwayat-istri" aria-selected="false">Riwayat Suami/Istri</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="anak-tab" data-toggle="tab" href="#riwayat-anak" role="tab" aria-controls="riwayat-anak" aria-selected="false">Riwayat Anak</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="ortu-tab" data-toggle="tab" href="#riwayat-ortu" role="tab" aria-controls="riwayat-ortu" aria-selected="false">Riwayat Orang Tua</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="saudara-tab" data-toggle="tab" href="#riwayat-saudara" role="tab" aria-controls="riwayat-saudara" aria-selected="false">Riwayat Saudara</a>
									</li>
								</ul>
								<p></p>
							<div class="tab-content" id="myTabContent">
								<div class="tab-pane fade show active" id="data-pribadi" role="tabpanel" aria-labelledby="home-tab">
									@include('pages.pegawai.data_pribadi',['pegawai'=>$pegawai])
								</div>
								<div class="tab-pane fade show" id="pendidikan-formal" role="tabpanel" aria-labelledby="profile-tab">
									@include('pages.riwayat.pendidikan.profile_index',['pegawai'=>$pegawai])
								</div>
								<div class="tab-pane fade show" id="non-formal" role="tabpanel" aria-labelledby="contact-tab">
									@include('pages.riwayat.nonformal.profile_index',['pegawai'=>$pegawai])
								</div>
								<div class="tab-pane fade show" id="riwayat-kepangkatan" role="tabpanel" aria-labelledby="pangkat-tab">
									@include('pages.riwayat.pangkat.profile_index',['pegawai'=>$pegawai])
								</div>
								<div class="tab-pane fade show" id="riwayat-jabatan" role="tabpanel" aria-labelledby="jabatan-tab">
									@include('pages.riwayat.jabatan.profile_index',['pegawai'=>$pegawai])
								</div>
								<div class="tab-pane fade show" id="riwayat-hukuman" role="tabpanel" aria-labelledby="hukuman-tab">
									@include('pages.riwayat.hukuman.profile_index',['pegawai'=>$pegawai])
								</div>
								<div class="tab-pane fade show" id="diklat-struktural" role="tabpanel" aria-labelledby="diklat-struktural-tab">
									@include('pages.riwayat.diklat.profile_index',['pegawai'=>$pegawai,'jenis'=>'STRUKTURAL'])
								</div>
								<div class="tab-pane fade show" id="diklat-fungsional" role="tabpanel" aria-labelledby="diklat-fungsional-tab">
									@include('pages.riwayat.diklat.profile_index',['pegawai'=>$pegawai,'jenis'=>'FUNGSIONAL'])
								</div>
								<div class="tab-pane fade show" id="diklat-teknis" role="tabpanel" aria-labelledby="diklat-teknis-tab">
									@include('pages.riwayat.diklat.profile_index',['pegawai'=>$pegawai,'jenis'=>'TEKNIS'])
								</div>
								<div class="tab-pane fade show" id="riwayat-istri" role="tabpanel" aria-labelledby="istri-tab">
									@include('pages.riwayat.keluarga.profile_index',['pegawai'=>$pegawai,'jenis'=>1])
								</div>
								<div class="tab-pane fade show" id="riwayat-anak" role="tabpanel" aria-labelledby="anak-tab">
									@include('pages.riwayat.keluarga.profile_index',['pegawai'=>$pegawai,'jenis'=>2])
								</div>
								<div class="tab-pane fade show" id="riwayat-ortu" role="tabpanel" aria-labelledby="ortu-tab">
									@include('pages.riwayat.keluarga.profile_index',['pegawai'=>$pegawai,'jenis'=>3])
								</div>
								<div class="tab-pane fade show" id="riwayat-saudara" role="tabpanel" aria-labelledby="saudara-tab">
									@include('pages.riwayat.keluarga.profile_index',['pegawai'=>$pegawai,'jenis'=>4])
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('modal')
<div class="modal fade" id="modal-diklat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Form Diklat</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body body-diklat">
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-outline-info proses-button" onclick="doSaveDiklat()" id="simpan">SIMPAN</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-keluarga" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Form Keluarga</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body body-keluarga">
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-outline-info proses-button" onclick="doSaveKeluarga()" id="simpan">SIMPAN</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')

	<script>
		function openDiklat(id,jenis)
		{
			$('.body-diklat').html('');
			$.post("{{ URL('riwayat_diklat/form') }}/"+id,{pegawai_id:'{{ $pegawai->id }}',jenis:jenis}).done(function(resp){
				$('.body-diklat').html(resp);
				$('#modal-diklat').modal('show')
			}).fail(function(xhr){
				alert(xhr.responseText)
			})
		
		}
		
		function doSaveDiklat()
		{
			var form = $('#form-diklat');
			if (form.valid()){
				$.post("{{ URL('riwayat_diklat/save') }}",form.serialize()).done(function(resp){
					if (resp == 'success'){
						Toast.fire({
							icon:'success',
							title:"Data Berhasil Disimpan/Diubah dan Menunggu Acc Admin"
						})
						$('#modal-diklat').modal('hide')
					} else {
						Toast.fire({
							icon:'error',
							title:resp
						})
					}
				}).fail(function(xhr){
					
					alert(xhr.responseText);
				})
			}
		}
		
		function openKeluarga(id,jenis)
		{
			$('.body-keluarga').html('');
			$.post("{{ URL('riwayat_keluarga/form') }}/"+id,{pegawai_id:'{{ $pegawai->id }}',jenis:jenis}).done(function(resp){
				$('.body-keluarga').html(resp);
				$('#modal-keluarga').modal('show')
			}).fail(function(xhr){
				alert(xhr.responseText)
			})
		
		}
		
		function doSaveKeluarga()
		{
			var form = $('#form-keluarga');
			if (form.valid()){
				$.post("{{ URL('riwayat_keluarga/save') }}",form.serialize()).done(function(resp){
					if (resp == 'success'){
						Toast.fire({
							icon:'success',
							title:"Data Berhasil Disimpan/Diubah dan Menunggu Acc Admin"
						})
						$('#modal-keluarga').modal('hide')
					} else {
						Toast.fire({
							icon:'error',
							title:resp
						})
					}
				}).fail(function(xhr){
					
					alert(xhr.responseText);
				})
			}
		}
	</script>
@endpush
	