
@push('styles')
  <link href="{{ asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@endpush
<a class="btn btn-primary float-right" href="{{ URL('pegawai/form/'._get('satuan_kerja')) }}"><i class="fa fa-plus"></i> Tambah</a>
<div class="card-box table-responsive">
	<table class="table table-striped table-bordered dt-responsive nowrap" id="myTable" cellspacing="0" width="100%">
		<thead>
	     <tr>
				<th rowspan="2">PILIHAN</th>
				<th rowspan="2">NAMA/TEMPAT TGL LAHIR</th>
				<th rowspan="2">NIP</th>
				<th colspan="2">PANGKAT</th>
				<th colspan="2">JABATAN</th>
				<th colspan="2">PEGAWAI</th>
				<th colspan="2">MASA KERJA</th>
				
	     </tr>
	     <tr>
				<th>GOL</th>
				<th>TMT GOL</th>
				<th>Nama</th>
				<th>TMT</th>
				<th>STATUS</th>
				<th>TMT</th>
				<th>THN</th>
				<th>BLN</th>
			</tr>
	  </thead>
		
	</table>
</div>

@push('scripts')
	<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script>
		$(function(){
			table = $('#myTable').DataTable({
				"processing": true,
				"serverSide": true,
				"ajax": {
					"url": "{{ URL('pegawai/data') }}",
					"type":"POST",
					"data": function ( d ) {
						d.satuan_kerja = "{{ _get('satuan_kerja') }}"
					}
				},
				columns:[
					{ "data": "aksi","orderable":false,"render":function(data,type,row){
						var button = '<a href="{{ URL('pegawai/form/') }}/'+row.satuan_kerja_id+'/?pegawai_id='+row.id+'" class="btn btn-outline-info btn-sm" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;'; 
						    button += '<a href="{{ URL('pegawai/detail') }}/'+row.id+'" class="btn btn-outline-success btn-sm"><i class="fa fa-search"></i></a>';
								
							return button;
					}},
					{ "data": "nama" ,"render":function(data,type,row){
						return row.nama_gelar_depan+"."+data+", "+row.nama_gelar_belakang+" ( "+row.tanggal_lahir+" )"
					}},
					{ "data": "nip" },
					{ "data": "nama_golongan" },
					{ "data": "gol_akhir_tmt" },
					{ "data": "nama_jabatan","render":function(data,type,row){
						if(row.jenis == 'STRUKTURAL'){
							return data;
						} else {
							return row.nama_jabatan2;
						}
					}},
					{ "data": "jabatan_tmt" },
					{ "data": "status_kepegawaian" },
					{ "data": "cpns_tmt","render":function(data,type,row){
						if (row.status_kepegawaian == "CPNS"){
							return data;
						} else if(row.status_kepegawaian == "CPNS"){
							return row.pns_tmt;
						} else {
							return "";
						}
					}},
					{ "data": "kerja_tahun" },
					{ "data": "kerja_bulan" },
				],
				"order": [[1, 'asc']],
			});
		})
	</script>
@endpush