<table class="table table-bordered">
	<tr>
		<td class="bg-secondary text-white">Tempat, Tanggal Lahir</td>
		<td colspan="5">{{ $pegawai->tempat_lahir }}, {{ tanggal_sql($pegawai->tanggal_lahir) }}</td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">Jenis Kelamin</td>
		<td colspan="5">{{ $pegawai->jenis_kelamin }}</td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">Status Perkawinan</td>
		<td colspan="5">{{ $pegawai->status_perkawinan }}</td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">Agama</td>
		<td colspan="5">{{ $pegawai->agama }}</td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">Status ASN</td>
		<td>{{ $pegawai->status_kepegawaian }}</td>
		<td class="bg-secondary text-white">TMT CPNS</td>
		<td>{{ format_tanggal_indonesia($pegawai->cpns_tmt) }}</td>
		<td class="bg-secondary text-white">TMT PNS</td>
		<td>{{ $pegawai->pns_tmt }}</td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">Pendidikan Awal</td>
		<td>{{ $pegawai->pendidikan_awal->nama }}</td>
		<td class="bg-secondary text-white">Tahun Pendidikan Awal</td>
		<td colspan="3">{{ $pegawai->pend_awal_th }}</td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">Pendidikan Akhir</td>
		<td>{{ $pegawai->pendidikan_akhir->nama }}</td>
		<td class="bg-secondary text-white">Tahun Pendidikan Akhir</td>
		<td colspan="3">{{ $pegawai->pend_akhir_th }}</td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">Jenis Jabatan</td>
		<td colspan="5">{{ $pegawai->jabatan->jenis }}</td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">Eselon</td>
		<td colspan="5">{{ @$pegawai->jabatan->eselon->nama }}</td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">Nama Jabatan</td>
		<td>{{ $pegawai->jabatan->nama }}</td>
		<td class="bg-secondary text-white">TMT Jabatan</td>
		<td colspan="3">{{ format_tanggal_indonesia($pegawai->jabatan_tmt) }}</td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">SOPD</td>
		<td colspan="5">{{ $pegawai->satuan_kerja->nama }}</td>
	</tr>
		<tr>
		<td class="bg-secondary text-white">Instansi (jika dipekerjakan)</td>
		<td colspan="5">{{ $pegawai->instansi_dpk }}</td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">Golongan Awal</td>
		<td>{{ $pegawai->golongan_awal->nama }},{{ $pegawai->golongan_awal->deskripsi }}</td>
		<td class="bg-secondary text-white">TMT Golongan Awal</td>
		<td colspan="3">{{ format_tanggal_indonesia($pegawai->gol_awal_tmt) }}</td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">Golongan Akhir</td>
		<td>{{ $pegawai->golongan_akhir->nama }},{{ $pegawai->golongan_akhir->deskripsi }}</td>
		<td class="bg-secondary text-white">TMT Golongan Akhir</td>
		<td colspan="3">{{ format_tanggal_indonesia($pegawai->gol_akhir_tmt) }}</td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">Masa Kerja Golongan</td>
		<td colspan="5">{{ $pegawai->kerja_tahun }} Tahun {{ $pegawai->kerja_bulan }} Bulan</td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">No. Karpeg</td>
		<td>{{ $pegawai->karpeg }}</td>
		<td class="bg-secondary text-white">No. Karis/Karsu</td>
		<td colspan="3">{{ $pegawai->karsutri }}</td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">No. Askes</td>
		<td colspan="5">{{ $pegawai->no_askes }} </td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">No. KTP</td>
		<td>{{ $pegawai->ktp }}</td>
		<td class="bg-secondary text-white">NPWP</td>
		<td colspan="3">{{ $pegawai->npwp }}</td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">Gol. Darah</td>
		<td colspan="5">{{ $pegawai->golongan_darah->nama }} </td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">Bapertarum</td>
		<td colspan="5">{{ $pegawai->bapertarum }} </td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">TMT Gaji Berkala Terbaru</td>
		<td colspan="5">{{ format_tanggal_indonesia($pegawai->tmt_kgb) }} </td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">Alamat Rumah</td>
		<td colspan="5">{{ $pegawai->alamat_rumah }} </td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">Kel./Desa</td>
		<td>{{ $pegawai->desa }}</td>
		<td class="bg-secondary text-white">Kecamatan</td>
		<td colspan="3">{{ $pegawai->kecamatan->nama }}</td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">Kab./Kota</td>
		<td>{{ $pegawai->kecamatan->kabupaten->nama }}</td>
		<td class="bg-secondary text-white">Kodepos</td>
		<td colspan="3">{{ $pegawai->kodepos }}</td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">Telp</td>
		<td>{{ $pegawai->telp }}</td>
		<td class="bg-secondary text-white">Hp</td>
		<td colspan="3">{{ $pegawai->telp_hp }}</td>
	</tr>
	<tr>
		<td class="bg-secondary text-white">Email</td>
		<td colspan="5">{{ $pegawai->email }} </td>
	</tr>
</table>

<style>
	.header {
		background-color:#4cffff
	}
</style>