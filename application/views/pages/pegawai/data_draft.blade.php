@extends('layouts.main')
@push('styles')
  <link href="{{ asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@endpush
@section('content')
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Data pegawai<small></small></h3>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Data Pegawai</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<form method="get" id="form-satker">
							<div class="form-group row">
								<label class="control-label col-md-3 col-sm-3 ">Pilih Satuan Kerja</label>
								<div class="col-md-6 col-sm-6 ">
									<select class="select2 form-control" tabindex="-1" name="satuan_kerja" required onchange="$('#form-satker').submit()">
										<option value="">Pilih Satuan Kerja</option>
										@foreach(App\Models\MsatuanKerja::where('status',1)->get() as $row)
										<option value="{{ $row->id }}" @if(_get('satuan_kerja') == $row->id) selected @endif >{{ $row->nama }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</form>
						
						@if(_get('satuan_kerja') != "")
							<div class="card-box table-responsive">
								<table class="table table-striped table-bordered dt-responsive nowrap" id="myTable" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Aksi</th>
										<th>Satuan Kerja</th>
										<th>Unit Kerja</th>
										<th>Nama</th>
										<th>Keterangan</th>
										<th>Status</th>
									</tr>
								</thead>	
								@forelse($rows as $row)
								<tr>
									<td>
									@if($row->status == "") 
										<a href="javascript:void(0)" onclick="accDraft({{ $row->id }})"><i class="fa fa-check"></i> Acc</a>
										<a href="{{ URL('pegawai/form/'.$row->satuan_kerja_id.'?draft_id='.$row->id) }}"><i class="fa fa-edit"></i> Edit</a>
										<a href="javascript:void(0)" onclick="delData({{ $row->id }})"><i class="fa fa-trash"></i> Hapus</a>
									@endif
									</td>
									<td>{{ $row->satuan_kerja->nama }}</td>
									<td>{{ $row->unit_kerja->nama }}</td>
									<td>{{ $row->nama }} ( {{ $row->nip }} )</td>
									<td>{{ $row->keterangan }}</td>
									<td>{{ $row->status }}</td>
								</tr>
								@empty
								@endforelse
							</table>
						</div>
						<script>
							$(function(){
								$('#myTable').DataTable();
							})
						</script>
						@endif
						
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('scripts')
	<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script>
		function accDraft(id)
		{
			Swal.fire({
				title: "Apakah Yakin akan Acc data draft ini ?",
				icon: "warning",
				showCancelButton: true,
				confirmButtonText: 'Ya',
			}).then(function(isConfirm){
				if (isConfirm.value) {
					$.post("{{ site_url('draft/acc') }}",{id:id}).done(function(result){
						if (result == 'success') {
							Toast.fire({icon:'success',title:"Data Berhasil Dihapus"})
							setTimeout(function(){
								location.reload();
							},2000);
						} else {
							Toast.fire({icon:'error',title:result})
						}
					}).fail(function(xhr){
						Toast.fire({icon:'error',title:xhr.responseText})
					})
				}
			});
		}
		
		function delData(id)
		{
			Swal.fire({
				title: "Apakah Yakin akan Menghapus data draft ini ?",
				icon: "warning",
				showCancelButton: true,
				confirmButtonText: 'Hapus',
			}).then(function(isConfirm){
				if (isConfirm.value) {
					$.post("{{ site_url('draft/hapus') }}",{id:id}).done(function(result){
						if (result == 'success') {
							
							Toast.fire({icon:'success',title:"Data Berhasil Dihapus"})
							setTimeout(function(){
								location.reload()
							},2000);
						} else {
							Toast.fire({icon:'error',title:result})
						}
					}).fail(function(xhr){
						Toast.fire({icon:'error',title:xhr.responseText})
					})
				}
			});
		}
	</script>
@endpush

