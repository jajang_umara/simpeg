@php 
	use App\Models\Mgolongan;
	use App\Models\Mkecamatan;
	use App\Models\MgolonganDarah;
	use App\Models\Magama;
	use App\Models\MtingkatPendidikan;
	
@endphp
@extends('layouts.main')
@section('content')
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Form Tambah/Edit Pegawai <small></small></h3>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	
	<div class="row">
		<div class="col-md-12 col-sm-12 ">
			<div class="x_panel">
				<div class="x_title">
					<h2>Tambah/Edit Pegawai {{ $satuanKerja->nama }}</h2>
					
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					@if(flash_get('error') == 1)
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						<strong>Error</strong> {{ flash_get("message") }}.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					@endif
					
					@if(flash_get('message'))
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						<strong>Berhasil</strong> Data Pegawai Berhasil ditambahkan ke Draft
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					@endif
					<form method="post" enctype="multipart/form-data" action="{{ URL('draft_pegawai/save_pegawai') }}">
						<input type="hidden" name="satuan_kerja_id" value="{{ $satuanKerja->id }}">
						<input type="hidden" name="draft_id" value="{{ @$draft_id }}">
						<input type="hidden" name="pegawai_id" value="{{ @$row->id }}">
						@if(isset($row->id) || isset($draft_id)) <input type="hidden" name="unit_kerja_id" value="{{ @$row->unit_kerja_id }}"> @endif
						<table class="table">
							<tr>
								<td>Unit Kerja</td>
								<td>:</td>
								<td colspan="4"><select class="form-control select2 col-md-6" name="unit_kerja_id" id="unit_kerja_id" @if(isset($row->id)) disabled @endif @if(isset($draft_id)) disabled @endif >
								<option value="0">-Tanpa Unit Kerja-</option>
								{!! option_unit_kerja($unitKerja,@$row->unit_kerja_id) !!}
								</select></td>
							</tr>
							<tr>
								<td>NIP</td>
								<td>:</td>
								<td width="40%"><input class="form-control col-md-9" type="text" name="nip" required value="{{ @$row->nip }}" @if(isset($row->id)) readonly @endif></td>
								<td>NIP Lama</td>
								<td>:</td>
								<td><input class="form-control col-md-9" type="text" name="nip_lama" value="{{ @$row->nip_lama }}"></td>
							</tr>
							<tr>
								<td>Nama Lengkap</td>
								<td>:</td>
								<td colspan="4"><input class="form-control col-md-6" type="text" name="nama" required value="{{ @$row->nama }}"></td>
							</tr>
							<tr>
								<td>Gelar Depan</td>
								<td>:</td>
								<td><input class="form-control col-md-6" type="text" name="nama_gelar_depan" value="{{ @$row->nama_gelar_depan }}"></td>
								<td>Gelar Belakang</td>
								<td>:</td>
								<td><input class="form-control" type="text" name="nama_gelar_belakang" value="{{ @$row->nama_gelar_belakang }}"></td>
							</tr>
							<tr>
								<td>Tempat Tanggal Lahir</td>
								<td>:</td>
								<td colspan="4"><input class="form-control col-md-3" type="text" name="tempat_lahir" required value="{{ @$row->tempat_lahir }}"><span class="col-md-1"></span>
									<input class="form-control col-md-3 datepicker" type="text" name="tanggal_lahir" placeholder="dd-mm-yyyy" data-inputmask="'mask': '99-99-9999'" required value="{{ tanggal_sql(@$row->tanggal_lahir) }}">
								</td>
							</tr>
							<tr>
								<td>Photo</td>
								<td>:</td>
								<td><input class="form-control col-md-8" type="file" name="photo"></td>
							</tr>
							<tr>
								<td>Jenis Kelamin</td>
								<td>:</td>
								<td colspan="4">@foreach(['L'=>'Laki-laki','P'=>'Perempuan'] as $k=>$v)<input type="radio" name="jenis_kelamin" value="{{ $k }}" required @if(isset($row) && $row->jenis_kelamin == $v) checked @endif> {{ $v}} @endforeach</td>
							</tr>
							<tr>
								<td>Status Perkawinan</td>
								<td>:</td>
								<td colspan="4">@foreach(['Kawin','Belum Kawin','Duda','Janda'] as $sts) <input type="radio" name="status_perkawinan" value="{{ $sts }}" required @if(isset($row) && $row->status_perkawinan == $sts) checked @endif> {{ $sts }} @endforeach</td>
							</tr>
							<tr>
								<td>Agama</td>
								<td>:</td>
								<td><select class="form-control col-md-8" name="agama" required>
									<option value="">-Pilih Agama-</option>
									@foreach(Magama::all() as $agm)
									<option value="{{ $agm->nama }}" @if(isset($row) && $row->agama == $agm->nama) selected @endif>{{ $agm->nama }}</option>
									@endforeach
									</select>
								</td>
							</tr>
							<tr>
								<td>Status Pegawai</td>
								<td>:</td>
								<td colspan="4">@foreach(['CPNS','PNS','PPPK'] as $sts) <input @if(isset($row) && $row->status_kepegawaian == $sts) checked @endif type="radio" name="status_kepegawaian" value="{{ $sts }}" onclick="statusPegawaiClick()" class="status-pegawai"> {{ $sts }} @endforeach</td>
							</tr>
							<tr>
								<td>TMT CPNS</td>
								<td>:</td>
								<td><input class="form-control col-md-6 datepicker" type="text" name="cpns_tmt" data-inputmask="'mask':'99-99-9999'" placeholder="dd-mm-yyyy" readonly id="cpns_tmt" value="{{ tanggal_sql(@$row->cpns_tmt) }}"></td>
								<td>TMT PNS</td>
								<td>:</td>
								<td><input class="form-control datepicker" type="text" name="pns_tmt" data-inputmask="'mask':'99-99-9999'" placeholder="dd-mm-yyyy" readonly id="pns_tmt" value="{{ tanggal_sql(@$row->pns_tmt) }}"></td>
							</tr>
							
							<tr>
								 <td>Pendidikan Awal</td>
								 <td>:</td>
								 <td>
										
									  <input type="text" id="_tingpend_awal" name="txt_tingpend_awal" value="" size="8" readonly class="form-control col-md-2"/>
									  <select id="id_pend_awal" name="pend_awal_id" class="form-control select2 col-md-8" onchange="changePendAwal()">
										<option value="">-Pilih Pendidikan-</option>
									  @foreach(MtingkatPendidikan::with('kategori.pendidikan')->get() as $rowx )
											
											
											@foreach($rowx->kategori as $kat)
												 <optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;{{ $kat->nama }}">
												 
												 @foreach ($kat->pendidikan as $pend)
													  <option value="{{ $pend->id }}" data-tingkat-pendidikan="{{ $rowx->nama }}" @if(isset($row)) @if($row->pend_awal_id == $pend->id) selected @endif @endif >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $pend->nama }}</option>
												 @endforeach
												 
											@endforeach
									  @endforeach
									  </select>
									  <br/>(<font color="red"><i>Pendidikan waktu diangkat menjadi CPNS </i></font>)
								 </td>
								 <td>Tahun Pendidikan Akhir</td>
								 <td>:</td>
								 <td><input type="text" name="pend_awal_th"  value="{{ @$row->pend_awal_th }}" maxlength="4" size="4" class="form-control"></td>
							</tr>
							<tr>
								 <td>Pendidikan Akhir</td>
								 <td>:</td>
								 <td>
										
									  <input type="text" id="_tingpend_akhir" name="txt_tingpend_akhir" value="" size="8" readonly class="form-control col-md-2"/>
									  <select id="id_pend_akhir" name="pend_akhir_id" class="form-control select2 col-md-8" onchange="changePendAkhir()">
										<option value="">-Pilih Pendidikan-</option>
									  @foreach(MtingkatPendidikan::with('kategori.pendidikan')->get() as $rowx )
											
											
											@foreach($rowx->kategori as $kat)
												 <optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;{{ $kat->nama }}">
												 
												 @foreach ($kat->pendidikan as $pend)
													  <option value="{{ $pend->id }}" data-tingkat-pendidikan="{{ $rowx->nama }}" @if(isset($row) && $row->pend_akhir_id == $pend->id) selected @endif>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $pend->nama }}</option>
												 @endforeach
												 
											@endforeach
									  @endforeach
									  </select>
									  <br/>(<font color="red"><i>Pendidikan sesuai ijazah terakhir</i></font>)
								 </td>
								 <td>Tahun Pendidikan Akhir</td>
								 <td>:</td>
								 <td><input type="text" name="pend_akhir_th" maxlength="4" size="4" class="form-control" value="{{ @$row->pend_akhir_th }}"></td>
							</tr>
						
							<tr>
								<td>Jenis Jabatan</td>
								<td>:</td>
								<td colspan="4">
									<select class="form-control col-md-6" name="jenis_jabatan" id="jenis_jabatan" onchange="jenisJabatan(this.value)" @if(isset($row->id)) disabled @endif @if(isset($draft_id)) disabled @endif >
									@foreach(jenis_jabatan() as $key=>$jab)
									<option value="{{ $key }}">{{ $jab }}</option>
									@endforeach
									</select>
								</td>
							</tr>
							<tr>
								<td>Jabatan</td>
								<td>:</td>
								<td colspan="4">
									<select class="form-control col-md-6" name="jabatan_id" id="jabatan_id" @if(isset($row->id)) disabled @endif @if(isset($draft_id)) disabled @endif >
										
									</select>
								</td>
							</tr>
							<tr>
								<td>TMT Jabatan</td>
								<td>:</td>
								<td><input class="form-control datepicker" type="text" name="jabatan_tmt" data-inputmask="'mask':'99-99-9999'" placeholder="dd-mm-yyyy" value="{{ tanggal_sql(@$row->jabatan_tmt) }}" @if(isset($row->id)) disabled @endif @if(isset($draft_id)) disabled @endif></td>
								<td colspan="2">@if(isset($row->id)) <button class="btn btn-info" onclick="riwayatJabatan()" type="button">Update Jabatan</button>@endif</td>
							</tr>
							<tr>
								<td>Angka Kredit</td>
								<td>:</td>
								<td colspan="4"><input class="form-control col-md-6" type="text" name="ak" value="{{ @$row->ak }}"></td>
							</tr>
							<tr>
							<td>Instansi (Jika Dipekerjakan)</td>
								<td>:</td>
								<td colspan="4"><input class="form-control col-md-6" type="text" name="instansi_dpk" value="{{ @$row->instansi_dpk }}"></td>
							</tr>
							<tr>
								<td>Golongan Awal</td>
								<td>:</td>
								<td><select class="form-control col-md-6" name="gol_awal_id" @if(isset($row->id)) disabled @endif @if(isset($draft_id)) disabled @endif>
											<option value="">-Pilih Golongan-</option>
											@foreach(Mgolongan::all() as $rowx)
											<option value="{{ $rowx->id }}" @if(isset($row) && $row->gol_awal_id == $rowx->id) selected @endif >{{ $rowx->nama }}</option>
											@endforeach
										</select>
								</td>
								<td>TMT Golongan Awal</td>
								<td>:</td>
								<td><input class="form-control datepicker" type="text" name="gol_awal_tmt" data-inputmask="'mask':'99-99-9999'" placeholder="dd-mm-yyyy" value="{{ tanggal_sql(@$row->gol_awal_tmt) }}" @if(isset($row->id)) readonly @endif @if(isset($draft_id)) disabled @endif></td>
							</tr>
							<tr>
								<td>Golongan Akhir</td>
								<td>:</td>
								<td><select class="form-control col-md-6" name="gol_akhir_id" @if(isset($row->id)) disabled @endif @if(isset($draft_id)) disabled @endif>
											<option value="">-Pilih Golongan-</option>
											@foreach(Mgolongan::all() as $rowx)
											<option value="{{ $rowx->id }}" @if(isset($row) && $row->gol_akhir_id == $rowx->id) selected @endif>{{ $rowx->nama }}</option>
											@endforeach
										</select>
								</td>
								<td>TMT Golongan Awal</td>
								<td>:</td>
								<td><input class="form-control datepicker" type="text" name="gol_akhir_tmt" data-inputmask="'mask':'99-99-9999'" placeholder="dd-mm-yyyy" value="{{ tanggal_sql(@$row->gol_akhir_tmt) }}" @if(isset($row->id)) readonly @endif @if(isset($draft_id)) disabled @endif></td>
							</tr>
							<tr>
								<td>Masa Kerja Golongan</td>
								<td>:</td>
								<td>
									<input class="form-control col-md-3" type="text" name="kerja_tahun" value="{{ @$row->kerja_tahun }}" @if(isset($row->id)) readonly @endif> 
									<label class="col-md-2">Tahun</label>
									<input class="form-control col-md-3" type="text" name="kerja_bulan" value="{{ @$row->kerja_bulan }}" @if(isset($row->id)) readonly @endif> 
									<label class="col-md-2">Bulan</label>
								</td>
								<td colspan="3">@if(isset($row->id)) <button class="btn btn-info" onclick="riwayatPangkat()" type="button">Update Pangkat</button>@endif</td>
							</tr>
							<tr>
								<td>No KARPEG</td>
								<td>:</td>
								<td><input class="form-control col-md-6" type="text" name="karpeg" value="{{ @$row->karpeg }}"></td>
								<td>No Karis/Karsu</td>
								<td>:</td>
								<td><input class="form-control" type="text" name="karsutri" value="{{ @$row->karsutri }}"></td>
							</tr>
							<tr>
								<td>No Askes</td>
								<td>:</td>
								<td colspan="4"><input class="form-control col-md-6" type="text" name="no_askes" value="{{ @$row->no_askes }}"></td>
							</tr>
							<tr>
								<td>No KTP</td>
								<td>:</td>
								<td><input class="form-control col-md-6" type="text" name="ktp" value="{{ @$row->ktp }}"></td>
								<td>NPWP</td>
								<td>:</td>
								<td><input class="form-control" type="text" name="npwp" value="{{ @$row->npwp }}"></td>
							</tr>
							<tr>
								<td>Golongan Darah</td>
								<td>:</td>
								<td colspan="4">
									<select class="form-control col-md-6" name="goldar_id">
									<option value="">-Pilih Gol darah-</option>
									@foreach(MgolonganDarah::all() as $rowx)
									<option value="{{ $rowx->id }}" @if(isset($row) && $row->goldar_id == $rowx->id) selected @endif >{{ $rowx->nama }}</option>
									@endforeach
									</select>
								</td>
							</tr>
							<tr>
								<td>BAPERTARUM</td>
								<td>:</td>
								<td>
									<select class="form-control col-md-6" name="bapertarum">
									<option value="">-Pilih-</option>
									@foreach(['Sudah Diambil','Belum Diambil'] as $bap)
									<option @if(isset($row) && $row->bapertarum == $bap) selected @endif >{{ $bap }}</option>
									@endforeach
									</select>
								</td>
							</tr>
							<tr>
								<td>TMT Gaji Berkala Terakhir</td>
								<td>:</td>
								<td colspan="4"><input class="form-control col-md-6 datepicker" type="text" name="tmt_kgb" data-inputmask="'mask':'99-99-9999'" placeholder="dd-mm-yyyy" value="{{ tanggal_sql(@$row->tmt_kgb) }}"></td>
							</tr>
							<tr>
								<td>Alamat Rumah</td>
								<td>:</td>
								<td colspan="4"><textarea class="form-control col-md-6" type="text" name="alamat_rumah">{{ @$row->alamat_rumah }}</textarea></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td colspan="4">
									<label class="col-md-3">Kel./Desa</label>
									<input class="form-control col-md-3" type="text" name="desa" value="{{ @$row->desa }}">
									<label class="col-md-3">Kecamatan</label>
									<select class="form-control col-md-3" onchange="getKabupaten()" name="kecamatan_id" id="kecamatan_id">
										<option value="">-Pilih Kecamatan-</option>
										@foreach(Mkecamatan::orderBy('nama')->get() as $rowx)
										<option value="{{ $rowx->id }}" data-kabupaten="{{ $rowx->kabupaten_id }}" @if(isset($row) && $row->kecamatan_id == $rowx->id) selected @endif >{{ $rowx->nama }}</option>
										@endforeach
									</select>
								</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td colspan="4">
									<label class="col-md-3">Kab./Kota</label>
									<input class="form-control col-md-3" type="text" readonly id="kabupaten">
									<label class="col-md-3">Kodepos</label>
									<input class="form-control col-md-3" type="text" name="kodepos" maxlength="5" value="{{ @$row->kodepos }}">
								</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td colspan="4">
									<label class="col-md-3">Telp</label>
									<input class="form-control col-md-3" type="text" name="telp" value="{{ @$row->telp }}">
									<label class="col-md-3">HP</label>
									<input class="form-control col-md-3" type="text" name="telp_hp" value="{{ @$row->telp_hp }}">
								</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td colspan="4">
									<label class="col-md-3">Email</label>
									<input class="form-control col-md-3" type="email" name="email" value="{{ @$row->email }}">
									
								</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td colspan="4">
									<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> SIMPAN</button>
									<button type="button" class="btn btn-warning" onclick="history.back()"><i class="fa fa-arrow-left"></i> BATAL</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('modal')
	@if(isset($row->id))
		@include('pages.riwayat.pangkat.form')
		@include('pages.riwayat.jabatan.form')
	@endif
@endsection

@push('scripts')
	<script>
		@if(isset($row))
			var jenis_jabatan = '{{ @get_jabatan($row->jabatan_id)->jenis }}'
			$('#jenis_jabatan').val(jenis_jabatan);
			jenisJabatan(jenis_jabatan);
			getKabupaten()
			changePendAwal()
			changePendAkhir()
			statusPegawaiClick()
		@endif
		
		function getKabupaten()
		{
			var value = $('#kecamatan_id option:selected').data('kabupaten');
			$.get("{{ URL('pegawai/get_kabupaten') }}/"+value).done(function(resp){
				$('#kabupaten').val(resp)
			})
		}
		
		function changePendAwal()
		{
			var tingkat = $('#id_pend_awal option:selected').data('tingkat-pendidikan');
			$('#_tingpend_awal').val(tingkat);
		}
		
		function changePendAkhir()
		{
			var tingkat = $('#id_pend_akhir option:selected').data('tingkat-pendidikan');
			$('#_tingpend_akhir').val(tingkat);
		}
		
		function statusPegawaiClick()
		{
			$('.status-pegawai').each(function(){
				if (this.checked){
					if (this.value == 'PNS'){
						$('#pns_tmt').prop('readonly',false);
						$('#cpns_tmt').prop('readonly',true);
						$('#cpns_tmt').val('');
					} else if (this.value == 'CPNS'){
						$('#pns_tmt').prop('readonly',true);
						$('#pns_tmt').val('');
						$('#cpns_tmt').prop('readonly',false);
					} else {
						$('#pns_tmt').prop('readonly',true);
						$('#cpns_tmt').prop('readonly',true);
						$('#pns_tmt').val('');
						$('#cpns_tmt').val('');
					}
				}
			})
		}
		
		function jenisJabatan(value)
		{
			$.post("{{ URL('jabatan/get_jabatan') }}",{jenis:value,satuan_kerja:"{{ $satuanKerja->id }}",unit_kerja:$('#unit_kerja_id').val()}).done(function(resp){
				resp = JSON.parse(resp)
				var jabatan = $('#jabatan_id');
				jabatan.empty();
				if (resp.length > 0){
					jabatan.append("<option value=''>-Pilih jabatan-</option>")
					$.each(resp,function(k,v){
						var selected = "";
						@if (isset($row))
							if ( {{ $row->jabatan_id}} == v.id){
								selected = 'selected';
							}
						@endif
						if (value == "STRUKTURAL"){
							jabatan.append("<option value='"+v.id+"' "+selected+">"+v.nama+"</option>")
						} else {
							jabatan.append("<option value='"+v.id+"' "+selected+" >"+v.jf.nama+"</option>")
						}
						
					})
				}
			})
		}
		
		function riwayatPangkat()
		{
			$('#form-riwayat-pangkat')[0].reset();
			$('#modal-riwayat-pangkat').modal('show');
		}
		
		function riwayatJabatan()
		{
			$('#form-riwayat-jabatan')[0].reset();
			$('#modal-riwayat-jabatan').modal('show');
		}
	</script>
@endpush