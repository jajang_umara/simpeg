<div class="col-md-3 left_col">
	<div class="left_col scroll-view">
	<div class="navbar nav_title" style="border: 0;">
		<a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>SIMPEG</span></a>
	</div>

	<div class="clearfix"></div>

	<!-- menu profile quick info -->
	<div class="profile clearfix">
		<div class="profile_pic">
			<img src="{{ asset('images/img.jpg') }}" alt="..." class="img-circle profile_img">
		</div>
		<div class="profile_info">
			<span>Welcome,</span>
			<h2>Admin</h2>
		</div>
	</div>
	<!-- /menu profile quick info -->

	<br />

	<!-- sidebar menu -->
	<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
		<div class="menu_section">
			<h3>MASTER</h3>
			<ul class="nav side-menu">
				<li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li><a href="index.html">Dashboard</a></li>
						<li><a href="index2.html">Dashboard2</a></li>
						<li><a href="index3.html">Dashboard3</a></li>
					</ul>
				</li>
				<li><a><i class="fa fa-copy"></i> Master <span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li><a href="{{ URL('bahasa') }}">Master Bahasa</a></li>
						<li><a href="{{ URL('satuan_kerja') }}">Master Satuan Kerja</a></li>
						<li><a href="{{ URL('unit_kerja') }}">Master Unit Kerja</a></li>
						<li><a href="{{ URL('jft/index/TERTENTU') }}">Master JFT</a></li>
						<li><a href="{{ URL('jft/index/UMUM') }}">Master JFU</a></li>
						<li><a href="{{ URL('jabatan') }}">Master Jabatan</a></li>
					</ul>
				</li>
				<li><a href="{{ URL('users') }}"><i class="fa fa-users"></i> Data Users</a></li>
			</ul>
		</div>
		<div class="menu_section">
			<h3>DATA</h3>
			<ul class="nav side-menu">
				<li><a href="{{ URL('pegawai') }}"><i class="fa fa-users"></i> Data Pegawai</a></li>
			
				<li><a><i class="fa fa-copy"></i> Data Draft <span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li><a href="{{ URL('draft_pegawai/data_pegawai') }}">Draft Pegawai</a></li>
						<li><a href="{{ URL('riwayat_pangkat/index/DRAFT') }}">Draft Riwayat Pangkat</a></li>
						<li><a href="{{ URL('riwayat_jabatan/index/DRAFT') }}">Draft Riwayat Jabatan</a></li>
						<li><a href="{{ URL('riwayat_pendidikan/index') }}">Draft Riwayat Pendidikan</a></li>
						<li><a href="{{ URL('riwayat_nonformal/index') }}">Draft Riwayat Pendidikan Non Formal</a></li>
						<li><a href="{{ URL('riwayat_hukuman/index') }}">Draft Riwayat Hukuman</a></li>
						<li><a href="{{ URL('riwayat_diklat/index/STRUKTURAL') }}">Draft Diklat Struktural</a></li>
						<li><a href="{{ URL('riwayat_keluarga/index/1') }}">Draft Riwayat Keluarga</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="menu_section">
			<h3>REKAP DAN GRAFIK</h3>
			<ul class="nav side-menu">
				<li><a><i class="fa fa-calendar"></i> Rekap <span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li><a href="{{ URL('rekap/skpd') }}">SKPD</a></li>
						<li><a href="{{ URL('rekap/jabatan') }}">Jabatan</a></li>
						<li><a href="{{ URL('rekap/golongan') }}">Golongan</a></li>
						<li><a href="{{ URL('rekap/jenis_kelamin') }}">Jenis Kelamin</a></li>
					</ul>
				</li>
			</ul>
		</div>

	</div>
<!-- /menu footer buttons -->
		<div class="sidebar-footer hidden-small">
			<a data-toggle="tooltip" data-placement="top" title="Settings">
				<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
			</a>
			<a data-toggle="tooltip" data-placement="top" title="FullScreen">
				<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
			</a>
			<a data-toggle="tooltip" data-placement="top" title="Lock">
				<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
			</a>
			<a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
				<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
			</a>
		</div>
		<!-- /menu footer buttons -->
	</div>
</div>