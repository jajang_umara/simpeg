@extends('layouts.main')
@section('content')
	<div class="">
		<div class="page-title">
			<div class="title_left">
				
			</div>
		</div>
		<div class="clearfix"></div>
		
		<div class="row">
			<div class="col-md-12 col-sm-12 ">
				<div class="x_panel">
					<div class="x_title">
						<center><h5>REKAPITULASI PEGAWAI NEGERI SIPIL DAERAH
							DI LINGKUNGAN PEMERINTAH KABUPATEN KERINCI
							TAHUN {{ date('Y') }}</h5></center>
						
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<table class="table table-bordered">
							<tr>
								<th rowspan="2">No.</th>
								<th rowspan="2" width="40%">Nama Skpd</th>
								<th colspan="{{ $golongan->count() }}"><center>Golongan</center></th>
								<th rowspan="2">Jumlah</th>
							</tr>
							<tr>
								@foreach($golongan->get() as $key=>$gol)
								<th>{{ $gol->nama }}</th>
								@endforeach
							</tr>
							@php $no=1; @endphp
							@forelse($rows as $row)
							<tr>
								<td>{{ $no }}</td>
								<td>{{ $row->nama }}</td>
								@php $jumlah = 0 @endphp
								@foreach($golongan->get() as $key=>$gol)
								@php $gols = 'golongan_'.$key; $jumlah += $row->{$gols} @endphp
								<td>{{ $row->{$gols} }}</td>
								@endforeach
								<td>{{ $jumlah }}</td>
								
							</tr>
							@if ($row->id == session_get('satuan_kerja',2))
								{!! total_golongan_unit_kerja(0,$row->id) !!}
							@endif
							@php $no += 1; @endphp
							@empty
							@endforelse
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection