@extends('layouts.main')
@section('content')
	<div class="">
		<div class="page-title">
			<div class="title_left">
				
			</div>
		</div>
		<div class="clearfix"></div>
		
		<div class="row">
			<div class="col-md-12 col-sm-12 ">
				<div class="x_panel">
					<div class="x_title">
						<center><h5>REKAPITULASI PEGAWAI NEGERI SIPIL DAERAH
							DI LINGKUNGAN PEMERINTAH KABUPATEN KERINCI
							TAHUN {{ date('Y') }}</h5></center>
						
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<table class="table table-bordered">
							<tr>
								<th>No.</th>
								<th>Nama Skpd</th>
								<th>Jumlah Pegawai</th>
							</tr>
							@php $no=1; @endphp
							@forelse($rows as $row)
							<tr>
								<td>{{ $no }}</td>
								<td>{{ $row->nama }}</td>
								<td>{{ $row->jumlah_pegawai }}</td>
							</tr>
							@if ($row->id == session_get('satuan_kerja',2))
								{!! total_pegawai_unit_kerja(App\Models\MunitKerja::where('satuan_kerja_id',$row->id)->where('parent',0)->with('childrens')->get()) !!}
							@endif
							@php $no += 1; @endphp
							@empty
							@endforelse
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection