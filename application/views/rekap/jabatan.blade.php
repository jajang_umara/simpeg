@extends('layouts.main')
@section('content')
	<div class="">
		<div class="page-title">
			<div class="title_left">
				
			</div>
		</div>
		<div class="clearfix"></div>
		
		<div class="row">
			<div class="col-md-12 col-sm-12 ">
				<div class="x_panel">
					<div class="x_title">
						<center><h5>REKAPITULASI PEGAWAI NEGERI SIPIL DAERAH
							DI LINGKUNGAN PEMERINTAH KABUPATEN KERINCI
							TAHUN {{ date('Y') }}</h5></center>
						
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<table class="table table-bordered">
							<tr>
								<th rowspan="2">No.</th>
								<th rowspan="2" width="40%">Nama Skpd</th>
								<th colspan="8"><center>Pejabat Struktural</center></th>
								<th rowspan="2">Fungsional Umum</th>
								<th rowspan="2">Fungsional Tertentu</th>
								<th rowspan="2">Jumlah Seluruh</th>
							</tr>
							<tr>
								<th>II.a</th>
								<th>II.b</th>
								<th>III.a</th>
								<th>III.b</th>
								<th>IV.a</th>
								<th>IV.b</th>
								<th>V.a</th>
								<th>Jumlah</th>
							</tr>
							@php $no=1; @endphp
							@forelse($rows as $row)
							<tr>
								<td>{{ $no }}</td>
								<td>{{ $row->nama }}</td>
								@foreach(range(0,6) as $nomor)
								@php $eselon = 'eselon_'.$nomor @endphp
								<td>{{ $row->{$eselon} }}</td>
								@endforeach
								<td>{{ $row->struktural }}</td>
								<td>{{ $row->umum }}</td>
								<td>{{ $row->tertentu }}</td>
								<td>{{  $row->struktural + $row->umum + $row->tertentu }}</td>
							</tr>
							@if ($row->id == session_get('satuan_kerja',2))
								{!! total_jabatan_unit_kerja(0,$row->id) !!}
							@endif
							@php $no += 1; @endphp
							@empty
							@endforelse
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection