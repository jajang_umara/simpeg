@extends('layouts.main')
@section('content')
	<div class="">
		<div class="page-title">
			<div class="title_left">
				
			</div>
		</div>
		<div class="clearfix"></div>
		
		<div class="row">
			<div class="col-md-12 col-sm-12 ">
				<div class="x_panel">
					<div class="x_title">
						<center><h5>REKAPITULASI PEGAWAI NEGERI SIPIL DAERAH
							DI LINGKUNGAN PEMERINTAH KABUPATEN KERINCI
							TAHUN {{ date('Y') }}</h5></center>
						
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<table class="table table-bordered">
							<tr>
								<th rowspan="2">No.</th>
								<th rowspan="2">Nama Skpd</th>
								<th colspan="2">Jenis Kelamin</th>
								<th rowspan="2">Jumlah Pegawai</th>
							</tr>
							<tr>
								<th>Laki-laki</th>
								<th>Perempuan</th>
							</tr>
							@php $no=1; @endphp
							@forelse($rows as $row)
							<tr>
								<td>{{ $no }}</td>
								<td>{{ $row->nama }}</td>
								<td>{{ $row->laki }}</td>
								<td>{{ $row->perempuan }}</td>
								<td>{{ $row->laki + $row->perempuan }}</td>
							</tr>
							@if ($row->id == session_get('satuan_kerja',2))
							{!! total_jenis_kelamin(0,$row->id) !!}
							@endif	
							@php $no += 1; @endphp
							
							@empty
							@endforelse
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection