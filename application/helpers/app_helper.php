<?php
use App\Library\Blade;
use App\Models\Mjabatan;

function _this()
{
	$ci =& get_instance();
	return $ci;
}

if (!function_exists('view')){
	function view($view,$data=array()){
		
		//View Path
		$path  = APPPATH.'views';
		
		// Blade instance
		$blade = new Blade($path,APPPATH.'cache/views');
		
		//Share Data
		$blade->share('title','Page Title');
		
		//Replace dot with slash
		$view  = str_replace('.','/',$view);
		
		echo $blade->make($view,$data);
	}
}

function _post($index,$default="")
{
	return isset($_POST[$index]) ? _this()->input->post($index,true) : $default;
}

function _get($index,$default="")
{
	return isset($_GET[$index]) ? _this()->input->get($index,true) : $default;
}

function session()
{
	return _this()->session;
}

function session_get($key,$default="")
{
	return _this()->session->userdata($key) ? _this()->session->userdata($key) : $default ;
}


function session_set($key,$value=null)
{
	if (is_array($key)){
		_this()->session->set_userdata($key);
	} else {
		_this()->session->set_userdata($key,$value);
	}
}

function flash_get($key,$json=false)
{
	if ($json){
		$json = json_decode(_this()->session->flashdata('akses'),true);
		return isset($json[$key]) ? $json[$key] : false;
	}
	return _this()->session->flashdata($key);
}


function flash_set($key,$value=null)
{
	if (is_array($key)){
		_this()->session->set_flashdata($key);
	} else {
		_this()->session->set_flashdata($key,$value);
	}
}

function load_db($db)
{
	return _this()->load->database($db,true);
}



function __($text)
{
	return ucfirst(str_replace("_"," ",$text));
}

function asset($asset)
{
	return base_url('assets/'.$asset);
}

function URL($url)
{
	return site_url($url);
}

function format_tanggal_indonesia($date,$hari=false)
{
	$bulan = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'Nopember','12'=>'Desember');
	$nama_hari  = array("Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu");
	
	$tanggal = "";
	if ($hari){
		$hari_num = date('N',strtotime($date));
		
		$tanggal .= $nama_hari[$hari_num-1].", ";
		
	}
	
	$split_tgl = explode("-",$date);
	$tanggal .= @$split_tgl[2]." ".@$bulan[$split_tgl[1]]." ".@$split_tgl[0];
	return $tanggal;
}

function tanggal_sql($tanggal)
{
	if ($tanggal == ""){
		return null;
	}
	$date = explode("-",$tanggal);
	return @$date[2]."-".@$date[1]."-".@$date[0];
}



function jenis_jabatan()
{
	return [""=>"--Pilih Jenis Jabatan--","STRUKTURAL"=>"Struktural","TERTENTU"=>"Jabatan Fungsional Tertentu","UMUM"=>"Jabatan Fungsional Umum"];
}

function get_jabatan($id)
{
	return Mjabatan::find($id);
}

function jenis_keluarga($jenis)
{
	$nama = "";
	switch($jenis){
		case 1 :
			$nama = "Istri/Suami";
			break;
		case 2 :
			$nama = 'Anak';
			break;
		case 3 :
			$nama = "Orang Tua";
			break;
		case 4 :
			$nama = "Saudara";
			break;
	}
	return $nama;
}