<?php
use App\Models\Mjabatan;
use App\Models\Mpegawai;
use App\Models\MunitKerja;
use App\Models\Mgolongan;

function render_unit_kerja($rows,&$html="",&$count=0)
{
	foreach($rows as $row){
		$html .= "<tr><td></td>";
		$html .= "<td>".str_repeat('&nbsp;',$count)."$row->nama</td>";
		$html .= "<td><button class='btn btn-primary btn-sm' onclick='loadForm(0,$row->id)'><i class='fa fa-plus'></i> Tambah Unit Kerja Bawahan</button>
								<button class='btn btn-success btn-sm' onclick='loadForm($row->id,$row->parent)'><i class='fa fa-edit'></i> Edit</button>
								<button class='btn btn-danger btn-sm' onclick='delData($row->id)'><i class='fa fa-trash'></i> Hapus</button></td></tr>";
		
		
		
		
		if (isset($row->childrens) && count($row->childrens) > 0){
			$count += 4;
			render_unit_kerja($row->childrens,$html,$count);
		} 
		
		if ($row->parent == 0){
			$count = 0;
		}
		
	}
	
	return $html;
}

function option_unit_kerja($rows,$id=null,&$html="",&$count=0)
{
	foreach($rows as $row){
		$selected = "";
		if ($row->id == $id){
			$selected = "selected";
		}
		$html .= "<option value='$row->id' $selected>";
		$html .= str_repeat('&nbsp;',$count).$row->nama;
		$html .= "</option>";
		
		if (isset($row->childrens) && count($row->childrens) > 0){
			$count += 4;
			option_unit_kerja($row->childrens,$id,$html,$count);
		} 
		
		if ($row->parent == 0){
			$count = 0;
		}
		
	}
	
	return $html;
}

function render_jabatan($rows,&$html="",&$count=4)
{
	foreach($rows as $row){
		$jab = Mjabatan::where('satuan_kerja_id',_get('satuan_kerja'))->where('unit_kerja_id',$row->id)->where("jenis","STRUKTURAL")->with('eselon')->first();
		
		$html .= "<tr><td>".str_repeat('&nbsp;',$count)."$row->nama<br>
						 ".str_repeat('&nbsp;',$count)."<button class='btn btn-primary btn-sm' onclick='loadForm(0,"._get('satuan_kerja').",$row->id)'><i class='fa fa-plus'></i></button>
						 </td>";
		$html .= "<td>" .($jab ? $jab->nama : "-")."(".($jab ? $jab->kode : "-").")"."(".($jab ? "eselon : ".$jab->eselon->nama : "-").")</td>";
		$html .= "<td></td>";
		$html .= "<td>";
		if ($jab){
			$html .= "<a href='#' onclick='loadForm($jab->id,$jab->satuan_kerja_id,$jab->unit_kerja_id)'><i class='fa fa-edit' ></i> Edit</a> | 
								<a href='#' onclick='delData($jab->id)'><i class='fa fa-trash' ></i> Hapus</a>";
		}
		$html .= "</td></tr>";
		
		$jabs = Mjabatan::where('satuan_kerja_id',_get('satuan_kerja'))->where('unit_kerja_id',$row->id)->where("jenis",'!=',"STRUKTURAL")->with('jf')->get();
		
		if ($jabs){
			foreach($jabs as $jb){
				$html .= "<tr><td></td><td>
										" .str_repeat('&nbsp;',4).$jb->jf->nama."(".$jb->jf->kode_jabatan.")
									</td>
									<td></td>
									<td><a href='#' onclick='delData($jb->id)'><i class='fa fa-trash' ></i> Hapus</a></td>
								</tr>";
			}
		}
		
		
		if (isset($row->childrens) && count($row->childrens) > 0){
			$count += 4;
			render_jabatan($row->childrens,$html,$count);
		} 
		
		if ($row->parent == 0){
			$count = 4;
		}
		
	}
	
	return $html;
}

function total_pegawai_unit_kerja($rows,&$html="",&$count=4)
{
	foreach($rows as $row){
		$html .= "<tr><td></td>";
		$html .= "<td>".str_repeat('&nbsp;',$count)."$row->nama</td>";
		$html .= "<td>".jumlah_pegawai($row->satuan_kerja_id,$row->id)."</td></tr>";
		
		
		
		
		if (isset($row->childrens) && count($row->childrens) > 0){
			$count += 4;
			total_pegawai_unit_kerja($row->childrens,$html,$count);
		} 
		
		if ($row->parent == 0){
			$count = 4;
		}
		
	}
	
	return $html;
}

function total_jabatan_unit_kerja($parent=0,$satuan_kerja,&$html="",&$count=4)
{
	$eselon = "";
	foreach (["II.a","II.b","III.a","III.b","IV.a","IV.b","V.a"] as $key=>$ese){
		$eselon .= "IF(master_eselon.nama = '$ese',COUNT(data_pegawai.nama),0) as eselon_$key,";
	}
	
	$eselon .= "IF(master_jabatan.jenis = 'STRUKTURAL',count(data_pegawai.nama),0) as struktural,";
	$eselon .= "IF(master_jabatan.jenis = 'UMUM',count(data_pegawai.nama),0) as umum,";
	$eselon .= "IF(master_jabatan.jenis = 'TERTENTU',count(data_pegawai.nama),0) as tertentu";
		
	$rows = MunitKerja::selectRaw("master_unit_kerja.nama,master_unit_kerja.id,$eselon")->leftJoin('data_pegawai',function($join){
		$join->on("data_pegawai.satuan_kerja_id","=","master_unit_kerja.satuan_kerja_id");
		$join->on("data_pegawai.unit_kerja_id","=","master_unit_kerja.id");
	})->leftJoin("master_jabatan","master_jabatan.id","=","data_pegawai.jabatan_id")
	  ->leftJoin("master_eselon","master_eselon.id","=","master_jabatan.eselon_id")
	  ->where("master_unit_kerja.parent",$parent)->where("master_unit_kerja.satuan_kerja_id",$satuan_kerja)
	  ->groupBy('master_unit_kerja.nama');
	if ($rows->count() > 0){
		$count += 4;
		foreach($rows->get() as $row){
			$html .= "<tr><td></td>";
			$html .= "<td>".str_repeat('&nbsp;',$count)."$row->nama</td>";
			foreach(range(0,6) as $nomor){
				$eselons = "eselon_$nomor";
				$html .= "<td>".$row->{$eselons}."</td>";
			}
			$html .= "<td>".$row->struktural."</td>";
			$html .= "<td>".$row->umum."</td>";
			$html .= "<td>".$row->tertentu."</td>";
			$html .= "<td>".($row->struktural+$row->umum+$row->tertentu)."</td>";
			$html .= "</tr>";
			
			total_jabatan_unit_kerja($row->id,$satuan_kerja,$html,$count);
		
		}
	} else {
		$count = 4;
	}
	
	
	return $html;
}

function total_golongan_unit_kerja($parent=0,$satuan_kerja,&$html="",&$count=4)
{
	$golongan = ",";
	foreach(Mgolongan::orderBy('nama')->get() as $key=>$r){
		$golongan .= "IF(master_golongan.nama = '$r->nama',COUNT(data_pegawai.nama),0) as golongan_$key,";
	}
	$golongan = substr($golongan,0,strlen($golongan)-1);
		
	$rows = MunitKerja::selectRaw("master_unit_kerja.nama,master_unit_kerja.id $golongan")->leftJoin('data_pegawai',function($join){
		$join->on("data_pegawai.satuan_kerja_id","=","master_unit_kerja.satuan_kerja_id");
		$join->on("data_pegawai.unit_kerja_id","=","master_unit_kerja.id");
	})->leftJoin("master_golongan","master_golongan.id","=","data_pegawai.gol_akhir_id")
	  ->where("master_unit_kerja.parent",$parent)->where("master_unit_kerja.satuan_kerja_id",$satuan_kerja)
	  ->groupBy('master_unit_kerja.nama');
	if ($rows->count() > 0){
		$count += 4;
		foreach($rows->get() as $row){
			$html .= "<tr><td></td>";
			$html .= "<td>".str_repeat('&nbsp;',$count)."$row->nama</td>";
			$jumlah = 0;
			foreach(Mgolongan::orderBy('nama')->get() as $key=>$r){
				$gols = "golongan_$key";
				$jumlah += $row->{$gols};
				$html .= "<td>".$row->{$gols}."</td>";
			}
			
			$html .= "<td>".($jumlah)."</td>";
			$html .= "</tr>";
			
			total_golongan_unit_kerja($row->id,$satuan_kerja,$html,$count);
		
		}
	} else {
		$count = 4;
	}
	
	
	return $html;
}

function total_jenis_kelamin($parent=0,$satuan_kerja,&$html="",&$count=4)
{
	$jk = "IF(data_pegawai.jenis_kelamin = 'L',COUNT(data_pegawai.nama),0) as laki,IF(data_pegawai.jenis_kelamin = 'P',COUNT(data_pegawai.nama),0) as perempuan";
	$rows = MunitKerja::selectRaw("master_unit_kerja.nama,master_unit_kerja.id,$jk")
		->leftJoin('data_pegawai',function($join){
			$join->on("data_pegawai.satuan_kerja_id","=","master_unit_kerja.satuan_kerja_id");
			$join->on("data_pegawai.unit_kerja_id","=","master_unit_kerja.id");
		})->where("master_unit_kerja.parent",$parent)->where("master_unit_kerja.satuan_kerja_id",$satuan_kerja)
			->groupBy('master_unit_kerja.nama');
	if ($rows->count() > 0){
		$count += 4;
		foreach($rows->get() as $row){
			$html .= "<tr><td></td>";
			$html .= "<td>".str_repeat('&nbsp;',$count)."$row->nama</td>";
			$html .= "<td>".$row->laki."</td>";
			$html .= "<td>".$row->perempuan."</td>";
			$html .= "<td>".($row->laki+$row->perempuan)."</td>";
			$html .= "</tr>";
			
			
			
			
			total_jenis_kelamin($row->id,$satuan_kerja,$html,$count);
		
		}
	} else {
		$count = 4;
	}
	
	
	return $html;
}


function jumlah_pegawai($satuan_kerja,$unit_kerja)
{
	return Mpegawai::where('satuan_kerja_id',$satuan_kerja)->where('unit_kerja_id',$unit_kerja)->count();
}