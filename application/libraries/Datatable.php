<?php
namespace App\Library;

class Datatable {
	
	private $ci;
	private $column_search = array();
	private $table;
	
	function __construct($table="",$column_search=array())
	{
		$this->ci =& get_instance();
		$this->column_search = $column_search;
		$this->table = $table;
	}
	
	private function _get_datatables_query($select,$join,$where)
	{
		$this->ci->db->select($select)->from($this->table);
		
		if (count($join) > 0){
			foreach($join as $jn){
				$j_type = isset($jn[2]) ? $jn[2] : 'left';
				$this->ci->db->join($jn[0],$jn[1],$j_type);
			}
		}
		
		$i = 0;
		if (isset($this->column_search)){
			foreach ($this->column_search as $item) // loop column 
			{
				if($_POST['search']['value']) // if datatable send POST for search
				{
					
					if($i===0) // first loop
					{
						$this->ci->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
						$this->ci->db->like($item, $_POST['search']['value']);
					}
					else
					{
						$this->ci->db->or_like($item, $_POST['search']['value']);
					}
					
					if(count($this->column_search) - 1 == $i) //last loop
					$this->ci->db->group_end(); //close bracket
				}
				$i++;
			}
		}
		
		$this->ci->db->where($where);
		
		$this->filter();
		$this->orderBy();
		
	}
	
	function orderBy()
	{
		if(isset($_POST['order'])) 
		{
			$this->ci->db->order_by($_POST['columns'][$_POST['order']['0']['column']]['data'], $_POST['order']['0']['dir']);
		} 
	}
	
	function limit_data()
	{
		if($_POST['length'] != -1) {
			$this->ci->db->limit($_POST['length'], $_POST['start']);
		}
	}
	
	function get_datatables($select="*",$join=array(),$where=array())
	{
		$this->_get_datatables_query($select,$join,$where);
		$this->limit_data();
		$query = $this->ci->db->get();
		
		$result['draw'] = intval( $_REQUEST['draw'] );
		$result['recordsFiltered'] = $this->count_filtered($select,$join,$where);
		$result['recordsTotal'] = $result['recordsFiltered'];
		$result['data'] = $query->result();
		return $result;
	}
	
	function filter()
	{
		$this->ci->db->where(array());
	}
	
	public function count_filtered($select,$join,$where)
	{
		$this->_get_datatables_query($select,$join,$where);
		$query = $this->ci->db->get();
		return $query->num_rows();
	}
	
	public function count_all()
	{
		$this->ci->db->from($this->table);
		return $this->ci->db->count_all_results();
	}
}