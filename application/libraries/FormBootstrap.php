<?php
namespace App\Library;

class FormBootstrap {
	
	static function inline($label,$input_type)
	{
		$input = "<div class='form-group'>";
		$input .= "<label>$label</label>";
		$input .= $input_type;
		$input .= "</div>";
		
		return $input;
	}
	
	static function input($label,$opts)
	{
		$input = "<input";
		foreach ($opts as $key=>$opt){
			$input .= " $key='$opt' ";
		}
		
		if (!array_key_exists('id',$opts)){
			$input .= " id='".$opts['name']."' ";
		}
		
		if (!array_key_exists('type',$opts)){
			$input .= " type='text' ";
		}
		
		if (!array_key_exists('placeholder',$opts)){
			$input .= " placeholder='$label' ";
		}
		
		$input .= "/>";
		return self::inline($label,$input);
	}
	
	static function textarea($label,$opts)
	{
		$input = "<textarea";
		foreach ($opts as $key=>$opt){
			if ($key == 'value') continue;
			$input .= " $key='$opt' ";
		}
		
		if (!array_key_exists('id',$opts)){
			$input .= " id='".$opts['name']."' ";
		}
		
		if (!array_key_exists('placeholder',$opts)){
			$input .= " placeholder='$label' ";
		}
		
		$input .= ">";
		$input .= @$opts['value'];
		$input .= "</textarea>"; 
		return self::inline($label,$input);
	}
	
	static function select($label,$opts)
	{
		$select = "<select";
		foreach ($opts as $key=>$opt){
			if (in_array($key,['options','value'])) continue;
			$select .= " $key='$opt' ";
		}
		
		if (!array_key_exists('id',$opts)){
			$select .= " id='".$opts['name']."' ";
		}
		$select .= ">";
		
		if (count($opts['options'])){
				foreach ($opts['options'] as $key=>$option) {
					$selected = "";
					if (isset($opts['value'])){
							if ($opts['value'] == $key) {
								$selected = 'selected';
							}
					}
					$select .= "<option value='$key' $selected>$option</option>";
				}
		}
		
		$select .= "</select>";
		return self::inline($label,$select);
	}
	
	static function hidden($opts)
	{
		$input = "<input type='hidden'";
		foreach ($opts as $key=>$opt){
			$input .= " $key='$opt' ";
		}
		$input .= "/>";
		
		return $input;
	}
}