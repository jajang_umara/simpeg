<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class Meselon extends Model
{
	protected $table = 'master_eselon';
	protected $guarded = ['id'];
}