<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class MunitKerja extends Model
{
	protected $table = 'master_unit_kerja';
	protected $guarded = ['id'];
	
	public function childrens()
	{
		return $this->hasMany('App\Models\MunitKerja', 'parent', 'id')->with('childrens');
	}
}