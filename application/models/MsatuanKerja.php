<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
use App\Models\Mgolongan;
class MsatuanKerja extends Model
{
	protected $table = 'master_satuan_kerja';
	protected $guarded = ['id'];
	
	function rekapPegawai()
	{
		return self::selectRaw("master_satuan_kerja.nama,master_satuan_kerja.id,COUNT(data_pegawai.nama) as jumlah_pegawai")->leftJoin('data_pegawai','data_pegawai.satuan_kerja_id','=','master_satuan_kerja.id')->groupBy('master_satuan_kerja.nama')->get();
	}
	
	function rekapJabatan()
	{
		$eselon = "";
		foreach (["II.a","II.b","III.a","III.b","IV.a","IV.b","V.a"] as $key=>$ese){
			$eselon .= "IF(master_eselon.nama = '$ese',COUNT(data_pegawai.nama),0) as eselon_$key,";
		}
		
		$eselon .= "IF(master_jabatan.jenis = 'STRUKTURAL',count(data_pegawai.nama),0) as struktural,";
		
		return self::selectRaw("$this->table.nama,$this->table.id,$eselon IF(master_jabatan.jenis = 'UMUM',COUNT(data_pegawai.nama),0) as umum,IF(master_jabatan.jenis = 'TERTENTU',COUNT(data_pegawai.nama),0) as tertentu")
							->leftJoin("data_pegawai","data_pegawai.satuan_kerja_id","=","$this->table.id")
							->leftJoin("master_jabatan","data_pegawai.jabatan_id","=","master_jabatan.id")
							->leftJoin("master_eselon","master_eselon.id","=","master_jabatan.eselon_id")
							->groupBy("master_satuan_kerja.nama")
							->get();
	}
	
	function rekapGolongan()
	{
		$golongan = ",";
		foreach(Mgolongan::orderBy('nama')->get() as $key=>$row){
			$golongan .= "IF(master_golongan.nama = '$row->nama',COUNT(data_pegawai.nama),0) as golongan_$key,";
		}
		$golongan = substr($golongan,0,strlen($golongan)-1);
		return self::selectRaw("$this->table.nama,$this->table.id $golongan")
							->leftJoin("data_pegawai","data_pegawai.satuan_kerja_id","=","$this->table.id")
							->leftJoin("master_golongan","data_pegawai.gol_akhir_id","=","master_golongan.id")
							->groupBy("master_satuan_kerja.nama")
							->get();
	}
	
	function rekapJenisKelamin()
	{
		return self::selectRaw("master_satuan_kerja.nama,master_satuan_kerja.id,IF(data_pegawai.jenis_kelamin = 'L',count(data_pegawai.nama),0) as laki,
									  IF(data_pegawai.jenis_kelamin = 'P',count(data_pegawai.nama),0) as perempuan")
						->leftJoin('data_pegawai','data_pegawai.satuan_kerja_id','=','master_satuan_kerja.id')->groupBy('master_satuan_kerja.nama')->get();
	}
}