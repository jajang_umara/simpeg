<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class MkategoriPendidikan extends Model
{
	protected $table = 'master_kategori_pendidikan';
	protected $guarded = ['id'];
	
	function pendidikan()
	{
		return $this->hasMany('App\Models\Mpendidikan','kategori_pendidikan_id','id');
	}
}