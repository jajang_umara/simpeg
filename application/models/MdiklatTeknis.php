<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class MdiklatTeknis extends Model
{
	protected $table = 'master_diklat_teknis';
	protected $guarded = ['id'];
}