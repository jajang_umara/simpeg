<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class MriwayatDiklat extends Model
{
	protected $table = 'riwayat_diklat';
	protected $guarded = ['id'];
	
	function struktural()
	{
		return $this->hasOne('App\Models\MdiklatStruktural','id','struktural_id');
	}
	
	function fungsional()
	{
		return $this->hasOne('App\Models\MdiklatFungsional','id','fungsional_id');
	}
	
	function teknis()
	{
		return $this->hasOne('App\Models\MdiklatTeknis','id','teknis_id');
	}
}