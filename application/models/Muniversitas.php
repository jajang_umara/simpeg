<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class Muniversitas extends Model
{
	protected $table = 'master_universitas';
	protected $guarded = ['id'];
}