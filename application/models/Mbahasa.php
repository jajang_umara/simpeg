<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class Mbahasa extends Model
{
	protected $table = 'master_bahasa';
	protected $guarded = ['id'];
}