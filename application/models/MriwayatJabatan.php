<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class MriwayatJabatan extends Model
{
	protected $table = 'riwayat_jabatan';
	protected $guarded = ['id'];
}