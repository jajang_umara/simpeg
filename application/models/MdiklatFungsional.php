<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class MdiklatFungsional extends Model
{
	protected $table = 'master_diklat_fungsional';
	protected $guarded = ['id'];
}