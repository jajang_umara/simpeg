<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class MrumpunJabatan extends Model
{
	protected $table = 'master_jabatan_rumpun';
	protected $guarded = ['id'];
}