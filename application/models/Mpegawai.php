<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class Mpegawai extends Model
{
	protected $table = 'data_pegawai';
	protected $guarded = ['id','nip'];
	
	function pendidikan_awal()
	{
		return $this->hasOne('App\Models\Mpendidikan','id','pend_awal_id');
	}
	
	function pendidikan_akhir()
	{
		return $this->hasOne('App\Models\Mpendidikan','id','pend_akhir_id');
	}
	
	function golongan_awal()
	{
		return $this->hasOne('App\Models\Mgolongan','id','gol_awal_id');
	}
	
	function golongan_akhir()
	{
		return $this->hasOne('App\Models\Mgolongan','id','gol_akhir_id');
	}
	
	function golongan_darah()
	{
		return $this->hasOne('App\Models\MgolonganDarah','id','goldar_id');
	}
	
	function jabatan()
	{
		return $this->hasOne('App\Models\Mjabatan','id','jabatan_id');
	}
	
	function kecamatan()
	{
		return $this->hasOne('App\Models\Mkecamatan','id','kecamatan_id');
	}
	
	function satuan_kerja()
	{
		return $this->hasOne('App\Models\MsatuanKerja','id','satuan_kerja_id');
	}
	
	function unit_kerja()
	{
		return $this->hasOne('App\Models\MunitKerja','id','unit_kerja_id');
	}
	
	public function getJenisKelaminAttribute($value)
  {
    return $value == 'L' ? 'Laki-laki' : 'Perempuan' ;
  }
}