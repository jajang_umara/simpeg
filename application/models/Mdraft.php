<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class Mdraft extends Model
{
	protected $table = 'data_draft';
	protected $guarded = ['id'];
	
	function satuan_kerja()
	{
		return $this->hasOne('App\Models\MsatuanKerja','id','satuan_kerja_id');
	}
	
	function unit_kerja()
	{
		return $this->hasOne('App\Models\MunitKerja','id','unit_kerja_id');
	}
}