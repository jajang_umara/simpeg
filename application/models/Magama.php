<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class Magama extends Model
{
	protected $table = 'master_agama';
	protected $guarded = ['id'];
}