<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class MdiklatStruktural extends Model
{
	protected $table = 'master_diklat_struktural';
	protected $guarded = ['id'];
}