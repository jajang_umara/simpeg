<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class Mjurusan extends Model
{
	protected $table = 'master_jurusan';
	protected $guarded = ['id'];
}