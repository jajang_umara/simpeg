<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class MriwayatPendidikan extends Model
{
	protected $table = 'riwayat_pendidikan';
	protected $guarded = ['id'];
	
	function tingkat_pendidikan()
	{
		return $this->hasOne('App\Models\MtingkatPendidikan','id','tingkat_pendidikan_id');
	}
	
	function jurusan()
	{
		return $this->hasOne('App\Models\Mjurusan','id','jurusan_id');
	}
	
	function fakultas()
	{
		return $this->hasOne('App\Models\Mfakultas','id','fakultas_id');
	}
}