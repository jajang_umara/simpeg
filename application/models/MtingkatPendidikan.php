<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class MtingkatPendidikan extends Model
{
	protected $table = 'master_tingkat_pendidikan';
	protected $guarded = ['id'];
	
	function kategori()
	{
		return $this->hasMany('App\Models\MkategoriPendidikan','tingkat_pendidikan_id','id');
	}
}