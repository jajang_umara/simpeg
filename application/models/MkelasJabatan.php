<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class MkelasJabatan extends Model
{
	protected $table = 'master_jabatan_kelas';
	protected $guarded = ['id'];
}