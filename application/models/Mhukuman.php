<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class Mhukuman extends Model
{
	protected $table = 'master_hukuman';
	protected $guarded = ['id'];
}