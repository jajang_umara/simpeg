<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class Mkabupaten extends Model
{
	protected $table = 'master_kabupaten';
	protected $guarded = ['id'];
}