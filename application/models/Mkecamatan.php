<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class Mkecamatan extends Model
{
	protected $table = 'master_kecamatan';
	protected $guarded = ['id'];
	
	function kabupaten()
	{
		return $this->belongsTo('App\Models\Mkabupaten','kabupaten_id','id');
	}
}