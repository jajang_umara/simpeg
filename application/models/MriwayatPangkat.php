<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class MriwayatPangkat extends Model
{
	protected $table = 'riwayat_pangkat';
	protected $guarded = ['id'];
	
	function golongan()
	{
		return $this->hasOne('App\Models\Mgolongan','id','golongan_id');
	}
	
	function unit_kerja()
	{
		return $this->hasOne('App\Models\MunitKerja','id','unit_kerja_id');
	}
}