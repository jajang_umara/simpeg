<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class Mfakultas extends Model
{
	protected $table = 'master_fakultas';
	protected $guarded = ['id'];
}