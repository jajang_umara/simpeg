<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class MriwayatHukuman extends Model
{
	protected $table = 'riwayat_hukuman';
	protected $guarded = ['id'];
	
	function hukuman()
	{
		return $this->hasOne('App\Models\Mhukuman','id','hukuman_id');
	}
}