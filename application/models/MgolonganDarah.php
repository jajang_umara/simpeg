<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class MgolonganDarah extends Model
{
	protected $table = 'master_golongan_darah';
	protected $guarded = ['id'];
}