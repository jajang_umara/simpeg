<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
class Mjabatan extends Model
{
	protected $table = 'master_jabatan';
	protected $guarded = ['id'];
	
	function eselon()
	{
		return $this->hasOne("App\Models\Meselon","id","eselon_id");
	}
	
	function jf()
	{
		return $this->hasOne("App\Models\Mjft","id","jf_id");
	}
	
	function getNamaAttribute($value)
	{
		if ($this->jenis == 'STRUKTURAL'){
			return $value;
		} else {
			return @$this->jf->nama;
		}
	}
	
	
}